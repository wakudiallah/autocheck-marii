<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRVehicleAssessmentsTable extends Migration
{
    /**
     * Run the migrations.   
     *
     * @return void 

     */
    public function up()
    {
        if(!Schema::hasTable('r_vehicle_assessments'))
        {
            Schema::create('r_vehicle_assessments', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('report_id', 40)->nullable();
                $table->string('driverSeatEvaluation', 80)->nullable();
                $table->string('driverSeatGoalAverage', 80)->nullable();
                $table->string('frontPassengerSeatPoints', 80)->nullable();
                $table->string('frontPassengerSeatEvaluation', 80)->nullable();
                $table->string('frontPassengerSeatGoalAverage', 80)->nullable();
                $table->string('dryRoadStoppingDistance', 80)->nullable();
                $table->string('wetRoadStoppingDistance', 80)->nullable();
                $table->string('safetyGrade', 80)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_vehicle_assessments');
    }
}
