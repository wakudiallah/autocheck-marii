<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_suppliers'))
        {
            Schema::create('parameter_suppliers', function (Blueprint $table) {
                $table->increments('id');
                $table->string('supplier', 35)->nullable();
                $table->text('desc')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->Integer('created_by')->nullable();
                $table->Integer('updated_by')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_suppliers');
    }
}
