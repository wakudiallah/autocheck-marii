<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('detail_buyers'))
        {
            Schema::create('detail_buyers', function (Blueprint $table) {
                $table->increments('id');

                $table->string('ic')->nullable();
                $table->string('nickname')->nullable();

                $table->tinyInteger('gender')->nullable();
                $table->text('mailing_address')->nullable();
                $table->string('postcode',20)->nullable();
                $table->string('bandar',50)->nullable();
                $table->string('negeri',50)->nullable();
                $table->string('negara',50)->nullable();

                $table->text('office_address')->nullable();
                $table->text('home_address')->nullable();
                $table->string('account_bank',50)->nullable();
                $table->string('account_bank_number',50)->nullable();

                $table->string('balance',50)->nullable();
                $table->string('amount_last_transactions',50)->nullable();
                $table->DateTime('date_last_transactions')->nullable();
                $table->tinyInteger('status_transactions');
                $table->tinyInteger('status');

                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_buyers');
    }
}
