<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterCountryOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_country_origins'))
        {
            Schema::create('parameter_country_origins', function (Blueprint $table) {
                $table->increments('id');
                $table->string('country_origin', 35)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->Integer('created_by')->nullable();
                $table->Integer('updated_by')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_country_origins');
    }
}
