<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRVehicleDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('r_vehicle_details'))
        {
            Schema::create('r_vehicle_details', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('report_id', 40)->nullable();
                $table->string('chassisNumber', 80)->nullable();
                $table->string('make', 80)->nullable();
                $table->string('model', 80)->nullable();
                $table->string('grade', 200)->nullable();
                $table->string('manufactureDate', 80)->nullable();
                $table->string('body', 80)->nullable();
                $table->string('engine', 80)->nullable();
                $table->string('drive', 80)->nullable();
                $table->string('transmission', 80)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_vehicle_details');
    }
}
