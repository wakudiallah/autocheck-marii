<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiclePastsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    

    public function up()
    {
        if(!Schema::hasTable('vehicle_pasts'))
        {
            Schema::create('vehicle_pasts', function (Blueprint $table) {
                $table->increments('id');
                $table->text('id_past')->nullable();
                $table->string('vehicle_id_number',40)->nullable();
                $table->string('engine_number',30)->nullable();
                $table->string('engine_capacity', 20)->nullable();
                $table->string('supplier', 30)->nullable();
                $table->date('register_date')->nullable();
                $table->string('user_id',10)->nullable();
                $table->string('verified_by', 10)->nullable();
                $table->string('verified_type_id', 10)->nullable();
                $table->string('country_id', 10)->nullable();
                $table->string('model_id', 10)->nullable();
                $table->string('checking_fee', 10)->nullable();
                $table->string('payment_id', 10)->nullable();
                $table->string('status', 20)->nullable();
                $table->string('model_is_match', 2)->nullable();
                $table->string('engine_number_is_match', 2)->nullable();
                $table->string('engine_capacity_is_match', 2)->nullable();

                $table->string('register_date_is_match', 2)->nullable();
                $table->string('is_delete', 2)->nullable();
                $table->string('delete_user_id', 2)->nullable();
                $table->string('deletion_time', 2)->nullable();

                $table->date('last_modification_time')->nullable();
                $table->string('last_modifier_user_id', 2)->nullable();
                $table->date('creation_time')->nullable();

                $table->string('creator_user_id', 2)->nullable();
                $table->date('checking_date')->nullable();

                $table->date('verified_date')->nullable();

                $table->string('registration_number', 2)->nullable();
                $table->string('registration_number_is_match', 2)->nullable();
                $table->string('brand_is_match', 2)->nullable();

                $table->string('fuel_id', 2)->nullable();
                $table->string('vehicle_id_number_is_match', 2)->nullable();
                $table->string('fuel_is_match', 2)->nullable();

                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_pasts');
    }
}
