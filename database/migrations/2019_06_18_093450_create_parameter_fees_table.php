<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterFeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_fees'))
        {
            Schema::create('parameter_fees', function (Blueprint $table) {
                $table->increments('id');
                $table->double('fee', 13, 2)->nullable();
                $table->double('tax', 13, 2)->nullable();
                $table->tinyInteger('status')->nullable();
                $table->Integer('created_by')->nullable();
                $table->Integer('updated_by')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_fees');
    }
}
