<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('report_vehicles'))
        {
            Schema::create('report_vehicles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 50)->nullable();
                $table->string('report_id', 50)->nullable();
                $table->integer('is_ready')->nullable();
                $table->dateTime('creation_date')->nullable();
                $table->dateTime('due_date')->nullable();
                $table->integer('is_download_verifier')->nullable();
                $table->integer('is_download_user')->nullable();
                $table->integer('is_download_kastam')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_vehicles');
    }
}
