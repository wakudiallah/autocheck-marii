<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBatchSentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('batch_sents'))
        {
            Schema::create('batch_sents', function (Blueprint $table) {
                $table->increments('id');
                $table->string('unique_sent', 80)->nullable();
                $table->string('created_by')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batch_sents');
    }
}
