<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_histories'))
        {
            Schema::create('parameter_histories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_parameter_history', 10)->nullable();
                $table->text('history_name', 50)->nullable();
                $table->tinyInteger('status');
                $table->string('created_by', 10)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_histories');
    }
}
