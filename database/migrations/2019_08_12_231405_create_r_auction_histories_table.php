<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRAuctionHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
      
     */
    public function up()
    {
        if(!Schema::hasTable('r_auction_histories'))
        {
            Schema::create('r_auction_histories', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('report_id', 40)->nullable();
                $table->string('date', 80)->nullable();
                $table->string('lotNumber', 80)->nullable();
                $table->string('auction', 80)->nullable();
                $table->string('make', 80)->nullable();
                $table->string('model', 80)->nullable();
                $table->string('registrationDate', 80)->nullable();
                $table->string('mileage', 80)->nullable();
                $table->string('displacement', 80)->nullable();
                $table->string('transmission', 80)->nullable();
                $table->string('color', 80)->nullable();
                $table->string('body', 80)->nullable();
                $table->string('finalPrice', 80)->nullable();
                $table->string('result', 80)->nullable();
                $table->string('assessment', 80)->nullable();
                $table->string('images', 80)->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_auction_histories');
    }
}
