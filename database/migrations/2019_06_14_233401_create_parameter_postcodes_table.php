<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterPostcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_postcodes'))
        {
            Schema::create('parameter_postcodes', function (Blueprint $table) {
                $table->increments('id');
                $table->char('postcode', 5)->nullable();
                $table->string('area', 70)->nullable();
                $table->string('post_office', 30)->nullable();
                $table->char('state_code', 3)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_postcodes');
    }
}
