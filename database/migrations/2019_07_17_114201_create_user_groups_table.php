<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('user_groups'))
        {
            Schema::create('user_groups', function (Blueprint $table) {
                $table->increments('id');
                $table->string('user_id', 10)->nullable();
                $table->string('group_name', 50)->nullable();
                $table->double('balance', 13, 2)->nullable();
                $table->string('created_by', 10)->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_groups');
    }
}
