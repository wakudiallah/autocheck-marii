<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleApiKastamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_api_kastams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vehicle_id', 200)->nullable();
            $table->string('api_from', 100)->nullable();
            $table->Integer('is_test')->nullable();
            $table->string('car_id', 200)->nullable();
            $table->string('search_id', 200)->nullable();
            $table->string('status', 200)->nullable();
            $table->string('request_by', 200)->nullable();
            $table->string('update_by', 200)->nullable();

            $table->string('vehicle', 200)->nullable();
            $table->string('make', 200)->nullable();
            $table->string('model', 200)->nullable();
            $table->string('grade', 200)->nullable();
            $table->string('manufactureDate', 200)->nullable();
            $table->string('body', 200)->nullable();
            $table->string('engine', 200)->nullable();
            $table->string('drive', 200)->nullable();
            $table->string('transmission', 200)->nullable();
            $table->Integer('image')->nullable();
            $table->string('date_of_origin', 200)->nullable();
            $table->string('displacement_cc', 200)->nullable();
            $table->string('average_market', 200)->nullable();
            $table->timestamps();
            $table->SoftDeletes();
        });
    }

    /**


date of original
cc
average market

    1. Vin/ Chassis
2. Make
3. Model
4. Engine Code / Engine Number
5. Body
6. Car Grade
7. Date Of Manufacture
8. Date of original registration
9. Drive
10. Transmission
11. Displacement (cc)
12. Average market
13: Photo of the vehicle


    +carId: 0
      1. chassisNumber: "ANH20W-8311979"
      2. make: "TOYOTA"
      3. model: "Alphard"
      6. grade: "240X"
      7. manufactureDate: ""
      5. body: "DBA-ANH20W"
      4. engine: "2AZ-FE"
      9. drive: "FF(front engine, front-wheel drive)"
      11. transmission: "CVT"
      +image: "/search/img/catalog/10102070_201404x.jpg"

      
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_api_kastams');
    }
}
