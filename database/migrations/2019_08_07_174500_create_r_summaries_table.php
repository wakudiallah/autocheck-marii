<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
      +: true
    +: true
    +: true
    +: true
    +: true
    +: "1950000"
    +: 0
    +: 1
    +: "2019-08-02 17:59:43"

     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('r_summaries'))
        {
            Schema::create('r_summaries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 80)->nullable();
                $table->string('report_id', 40)->nullable();
                 $table->string('registered', 80)->nullable();
                $table->string('accident', 80)->nullable();
                $table->string('odometer', 80)->nullable();
                $table->string('recall', 80)->nullable();
                $table->string('safetyGrade', 80)->nullable();
                $table->string('averagePrice', 80)->nullable();
                $table->string('buyback', 80)->nullable();
                $table->string('contaminationRisk', 80)->nullable();
                $table->string('fillingTime', 80)->nullable();
                $table->timestamps();
            });
            }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('r_summaries');
    }
}
