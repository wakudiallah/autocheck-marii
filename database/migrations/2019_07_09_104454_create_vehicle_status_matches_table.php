<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleStatusMatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('vehicle_status_matches'))
        {
            Schema::create('vehicle_status_matches', function (Blueprint $table) {
                $table->increments('id');
                $table->string('id_vehicle', 100)->nullable();
                $table->string('vehicle', 100)->nullable();
                $table->tinyInteger('country_origin')->nullable();
                $table->tinyInteger('brand')->nullable();
                $table->tinyInteger('model')->nullable();
                $table->tinyInteger('engine_number')->nullable();
                $table->tinyInteger('cc')->nullable();
                $table->tinyInteger('fuel_type')->nullable();
                $table->tinyInteger('year_manufacture')->nullable();
                $table->tinyInteger('registation_date')->nullable();
                $table->tinyInteger('status')->nullable();
                $table->timestamps();
                $table->SoftDeletes();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_status_matches');
    }
}
