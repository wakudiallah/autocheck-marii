<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParameterStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('parameter_states'))
        {
        Schema::create('parameter_states', function (Blueprint $table) {
            $table->increments('id');
            $table->char('state_code', 3)->nullable();
            $table->string('state_name', 35)->nullable();
                $table->string('country', 100)->nullable();
            $table->timestamps();
        });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parameter_states');
    }
}
