<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/



//Route::get('/', 'WebController@index');
Route::get('/', 'Auth\LoginController@index');

//Route::get('/public', 'WebController@index');
Route::post('/check-vehicle-web', 'WebController@store');
Route::get('/sample_report', 'WebController@sample_report');

Route::get('/reset', function () {
    return view('auth.reset_password');
});

/*Route::get('/', function () {
    return view('auth.login');
});*/


Route::get('/send', function () {
    return view('web.register_email.register_email');
});


Route::get('qr-code-g', function () {
  \QrCode::size(500)
            ->format('png')
            ->generate('ItSolutionStuff.com', public_path('images/qrcode.png'));
  return view('admin.report.report_autocheck');
    
});


Auth::routes();



//Route::get('/home', 'HomeController@index')->name('home');
//Route::get('/login2', 'Auth\LoginController@login');
//Route::post('/login2', 'Auth\LoginController@login_post');


Route::get('/authenticated', 'Auth\LoginController@authenticated');

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/register-user', 'Auth\RegisterController@register_user');

Route::post('/register-buyer', 'BuyerController@store');


Route::group( ['middleware' => ['auth']], function() {

	
	Route::get('/kastam/vehicle', 'VehicleCheckedController@kastam_vehicle_complete');
	Route::get('/car-dealer/vehicle', 'VehicleCheckedController@car_dealer_vehicle_complete');
	Route::get('/buyer/vehicle', 'VehicleCheckedController@buyer_vehicle_complete');


	/* Admin */
	Route::get('/home', 'DashboardController@index')->name('home');
	
	/* Dashboard */
	Route::get('/dashboard', 'DashboardController@index');


	//--- Search 
	Route::get('/search-vehicle', 'SearchController@index'); 
	Route::post('/search-vehicle', 'SearchController@search');
	Route::get('/search-payment', 'SearchController@show');
	

	//Pay
	Route::post('/pay-chassis', 'PayController@store');
	Route::any('callback', 'PayController@callback');

	//login buyer
	Route::post('/login-buyer', 'Auth\LoginController@buyer');


	Route::get('/vehicle-checked', 'VehicleCheckedController@index');  //buat controller baru
	Route::get('/open-vehicle-verify', 'VehicleCheckedController@open_vehicle');


	Route::get('user-profile', 'ProfileController@index');

	/* User */
	Route::get('user', 'UserController@index');
	Route::get('add-user', 'UserController@create');
	Route::post('save-user', 'UserController@store');
	Route::get('edit-user/{id}', 'UserController@edit');
	Route::post('update-user/{id}', 'UserController@update');
	Route::get('delete-user/{id}', 'UserController@destroy');

	/* User Group */
	Route::get('user-group', 'UserController@index_group');
	Route::post('save-user-group', 'UserController@store_group');
	Route::get('delete-user-group/{id}', 'UserController@destroy');
	Route::get('edit-user-group/{id}', 'UserController@edit_group');
	Route::post('update-user-group/{id}', 'UserController@update_group');
	

	/* Parameter */
	Route::get('role', 'RoleController@index');
	Route::post('save-role', 'RoleController@store');
	Route::get('edit-role/{id}', 'RoleController@edit');
	Route::post('update-role/{id}', 'RoleController@update');
	Route::get('delete-role/{id}', 'RoleController@destroy');


	Route::get('package', 'PackageController@index');
	Route::post('save-package', 'PackageController@store');
	Route::get('edit-package/{id}', 'PackageController@edit');
	Route::post('update-package/{id}', 'PackageController@update');
	Route::get('delete-package/{id}', 'PackageController@destroy');

	Route::get('email', 'EmailController@index');
	Route::post('save-email', 'EmailController@store');
	Route::get('edit-email/{id}', 'EmailController@edit');
	Route::post('update-email/{id}', 'EmailController@update');
	Route::get('delete-email/{id}', 'EmailController@destroy');


	Route::get('brand', 'BrandController@index');
	Route::post('save-brand', 'BrandController@store');
	Route::get('edit-brand/{id}', 'BrandController@edit');
	Route::post('update-brand/{id}', 'BrandController@update');
	Route::get('delete-brand/{id}', 'BrandController@destroy');

	Route::get('type-vehicle', 'TypeVehicleController@index');
	Route::post('save-type-vehicle', 'TypeVehicleController@store');
	Route::get('edit-type-vehicle/{id}', 'TypeVehicleController@edit');
	Route::post('update-type-vehicle/{id}', 'TypeVehicleController@update');
	Route::get('delete-type-vehicle/{id}', 'TypeVehicleController@destroy');


	Route::get('supplier', 'SupplierController@index');
	Route::post('save-supplier', 'SupplierController@store');
	Route::get('edit-supplier/{id}', 'SupplierController@edit');
	Route::post('update-supplier/{id}', 'SupplierController@update');
	Route::get('delete-supplier/{id}', 'SupplierController@destroy');
	

	Route::get('country-origin', 'CountryOriginController@index');
	Route::post('save-country-origin', 'CountryOriginController@store');
	Route::get('edit-country-origin/{id}', 'CountryOriginController@edit');
	Route::post('update-country-origin/{id}', 'CountryOriginController@update');
	Route::get('delete-country-origin/{id}', 'CountryOriginController@destroy');

	Route::get('fuel', 'FuelController@index');
	Route::post('save-fuel', 'FuelController@store');
	Route::get('edit-fuel/{id}', 'FuelController@edit');
	Route::post('update-fuel/{id}', 'FuelController@update');
	Route::get('delete-fuel/{id}', 'FuelController@destroy');


	Route::get('fee', 'FeeController@index');
	Route::post('save-fee', 'FeeController@store');
	Route::get('edit-fee/{id}', 'FeeController@edit');
	Route::post('update-fee/{id}', 'FeeController@update');
	Route::get('delete-fee/{id}', 'FeeController@destroy');


	Route::get('par-model', 'ModelController@index');
	Route::post('save-model', 'ModelController@store');
	Route::get('edit-model/{id}', 'ModelController@edit');
	Route::post('update-model/{id}', 'ModelController@update');
	Route::get('delete-model/{id}', 'ModelController@destroy');

	Route::get('group', 'UserGroupController@index');
	Route::post('save-group', 'UserGroupController@store');
	Route::get('edit-group/{id}', 'UserGroupController@edit');
	Route::post('update-group/{id}', 'UserGroupController@update');
	Route::get('delete-group/{id}', 'UserGroupController@destroy');

	Route::get('branch', 'BranchController@index');
	Route::post('save-branch', 'BranchController@store');
	Route::get('edit-branch/{id}', 'BranchController@edit');
	Route::post('update-branch/{id}', 'BranchController@update');
	Route::get('delete-branch/{id}', 'BranchController@destroy');
	

	Route::get('balance', 'BalanceController@index');
	Route::post('save-balance/{id}', 'BalanceController@store');
	Route::get('edit-balance/{id}', 'BalanceController@edit');
	Route::post('update-balance/{id}', 'BalanceController@update');
	Route::get('delete-balance/{id}', 'BalanceController@destroy');
	Route::get('history-balance/{id}', 'BalanceController@history');
	Route::get('topup-balance', 'BalanceController@topup');
	


	/* ========== NAZA / group ========== */
	Route::get('vehicle-checking', 'Naza\VehicleCheckingController@index');
	Route::post('save-vehicle-checking', 'Naza\VehicleCheckingController@store');
	Route::get('edit-vehicle-checking', 'Naza\VehicleCheckingController@edit');
	Route::post('update-vehicle-checking/{id}', 'Naza\VehicleCheckingController@update');
	Route::get('delete-vehicle-checking', 'Naza\VehicleCheckingController@destroy');

	Route::get('sync/{id}', 'Naza\VehicleCheckingController@sync');
	Route::get('sync_autocheck3/{id}', 'Naza\VehicleCheckingController@sync_autocheck3');
	Route::get('sync_autocheck4', 'Naza\VehicleCheckingController@sync_shortreport_kastam');



	/* ============= Buyer ==============*/
	Route::post('/check-vehicle-buyer', 'Naza\VehicleCheckingController@search_vehicle');
	Route::post('/post-check-vehicle-buyer', 'Naza\VehicleCheckingController@post_search_vehicle');


	/* Report */

	Route::get('report-autocheck-kastam-api/{id}', 'ReportController@kastam_api');
	
	Route::get('report-autocheck/{id}', 'ReportController@index');
	Route::get('report-autocheck-mn/{id}', 'ReportController@report_mn');
	Route::get('report-carvx/{id}', 'ReportController@carvx');
	Route::get('report-autocheck-management', 'ReportController@buyer_group');
	Route::post('report-autocheck-management-view', 'ReportController@buyer_group_view');
	Route::get('report-summary', 'ReportController@summary_report');
	Route::post('report-summary-view', 'ReportController@summary_report_view');
	Route::post('report-summary-print', 'ReportController@summary_report_print');

	Route::get('report-vehicle', 'ReportController@report_vehicle');
	Route::post('report-vehicle-na-view', 'ReportController@report_vehicle_view');
	Route::post('report-vehicle-na-print', 'ReportController@report_vehicle_print');

	Route::get('report-short-past/{id}', 'ReportController@report_short_past');
	Route::get('report-monthly', 'ReportController@get_report_monthly');
	Route::post('report_monthly_view', 'ReportController@report_monthly_view');
	Route::post('report-monthly-print', 'ReportController@report_monthly_print');


	/* ======== shared ========= */
	Route::get('account', 'AccountController@index');
	Route::post('update-account/{id}', 'AccountController@update');
	Route::post('save/profile/{id}', 'AccountController@store');


	Route::get('balance-history', 'BalanceHistoryController@index');
	Route::get('history-vehicle/{id}', 'HistoryVehicleController@index');
	Route::get('/vehicle-past', 'VehiclePastController@index');
	


	Route::get('/postcode/{id}', 'ApiController@postcode');

	Route::get('/mo', 'ApiController@mo');



	/* ============ Verified =============== */
	Route::get('verification-tasklist', 'VehicleCheckedController@tasklist');
	Route::get('verification-tasklist-kastam', 'VehicleCheckedController@tasklist_kastam');
	Route::get('verification-shortreport-kastam', 'VehicleCheckedController@tasklist_kastam_shortreport');
	

	Route::get('sync_all', 'Naza\VehicleCheckingController@sync_all');
	
	Route::get('verify-vehicle/{id}', 'VehicleVerifyController@verify');
	Route::post('verify-vehicle-verifier-api/{id}', 'VehicleVerifyController@store_api');
	Route::post('verify-vehicle-verifier-manual/{id}', 'VehicleVerifyController@store_manual');
	Route::post('upload-report-vehicle/{id}', 'VehicleVerifyController@upload_full_report');

	/* sent-vehicle */
	Route::get('sent-vehicle', 'SentController@index');
	Route::get('sent-vehicle-kastam', 'SentController@sent_vehicle_kastam');
	Route::post('generate-and-sent-vehicle', 'SentController@generate_and_sent');
	Route::post('reject-before-sent-kadealer/{id}', 'SentController@reject_kadealer');
	Route::post('reject-before-sent-kastam/{id}', 'SentController@reject_kastam');

	Route::get('update-send/{id}', 'SentController@update');
	Route::get('batch-sent', 'SentController@batch');
		
	Route::get('not-found', 'RejectController@index');

});