@extends('admin.layout.template')

@section('content')

      

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Searching</h3>
              </div>
            </div>
            <!-- End Page Header -->
            

            <div class="row">
              <div class="col-lg-12 mb-4">
                
                <!-- Input & Button Groups -->
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Search</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      
                      <form id="search" action="{{url('search-vehicle')}}" method="post" enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="input-group mb-5">
                          <input type="text" class="form-control form-control-lg" placeholder="Vehicles" aria-label="Recipient's username" aria-describedby="basic-addon2" name="search">
                          <div class="input-group-append">
                            <input type="Submit" class="btn btn-white" value="Search">
                          </div>
                        </div>
                        
                      </form>
                    </li>
                    
                      
                  </ul>

                </div>
                <!-- / Input & Button Groups -->
              </div>

              
            </div>

          

            </div>
          </div>


@endsection


@push('js')


  

@endpush