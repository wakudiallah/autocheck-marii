<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('images/fav.ico')}}">


    <title>Autocheck</title>
  </head>
  <body>

    <h1>Balance</h1>
    
    <table class="table table-striped">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Vehicle</th>
          <th scope="col">Balance (RM)</th>
          <th scope="col">Fee (RM)</th>
          <th scope="col">Desc</th>
          <th scope="col">Top Up By</th>
          <th scope="col">Date</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 1; ?>
        @foreach($data as $data)
        <tr>
          <?php 
            $balance = number_format($data->balance, 2, '.', '');
            $transaction_fee = number_format($data->transaction_fee, 2, '.', '');
          ?>
          <td scope="row">{{$i++}}</td>
          <td>{{$data->vehicle}}</td>
          <td>RM {{$balance}}</td>
          <td>RM {{$transaction_fee}}</td>
          <td>{{$data->desc}}</td>
          <td>{{$data->topup_by->name}}</td>
          <td>{{$data->created_at}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>