@extends('web.layout2.template')

@section('content')


<style>
    .modal-backdrop {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: -100 !important;
    background-color: #000;
}
</style>


<section class="box-slider-search">


            <div class="container">

                <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="z-index: 50 !important;">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <img src="{{asset('images/VIN Search.jpg')}}" class="img-responsive">
            </div>
          </div>
        </div>

                <h1 class="nocontent outline">--- Search form  ---</h1>
                <div class="row">
                    <div class="col-md-7">
                          <div class="box_slider">
                            <h2 style="text-align: center !important; margin-bottom: 70px !important;"><b style="color: #8a4b4b">autocheck report</b> <br> is ESSENTIAL when shopping for  used imported car  </h2>
                          </div>
                        <div id="rev_slider-wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="classicslider1" style="margin-bottom: 40px !important">
                            <div class="tp-banner-container">
                                <div class="tp-banner" >
                                    <ul>                        
                                        <li data-transition="fade" data-slotamount="7" data-masterspeed="2000" 
                                            data-saveperformance="on"  data-title="Ken Burns Slide">
                                            <!-- MAIN IMAGE -->
                                            <img src="{{asset('images/slider.jpg')}}"  alt="2" data-lazyload="{{asset('images/slider.jpg')}}" 
                                                 data-bgposition="right top" data-kenburns="off" data-duration="12000" 
                                                 data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" 
                                                 data-bgpositionend="center bottom">

                                             <!--     
                                            <div class="tp-caption tentered_white_huge lft tp-resizeme" 
                                                 data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600"
                                                 data-y="130" data-hoffset="0" data-x="center"
                                                 style="">
                                                <img alt="" src="{{asset('web2/img/4.png')}}" style="width: 110px; height: 110px;">
                                            </div>
                                            <div class="tp-caption tentered_white_huge lft tp-resizeme" 
                                                 data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600"
                                                 data-y="272" data-hoffset="0" data-x="center"
                                                 style="color: #fff; text-transform: uppercase; font-size: 40px; letter-spacing: 6px;
                                                 font-weight: 400;">
                                                
                                            </div>
                                            <div class="tp-caption tentered_white_huge lfb tp-resizeme" data-endspeed="300" 
                                                 data-easing="Power4.easeOut" data-start="800" data-speed="600" data-y="320" 
                                                 data-hoffset="0" data-x="center"
                                                 style="color: #fff; font-size: 13px; text-transform: uppercase; letter-spacing: 10px;">
                                                <i class="fa fa-map-marker"> </i> 
                                            </div>
                                            <div class="tp-caption tentered_white_huge lft tp-resizeme" 
                                                 data-endspeed="300" data-easing="Power4.easeOut" data-start="400" data-speed="600"
                                                 data-y="365" data-hoffset="0" data-x="center"
                                                 style="color: #fff; text-transform: uppercase; font-size: 40px; letter-spacing: 6px;
                                                 font-family: Montserrat; font-weight: 400;">
                                               
                                            </div> -->
                                            

                                        </li> 


                                        
                                    </ul>
                                    <div class="tp-bannertimer"></div>
                                    <div>

                                    </div>
                                </div>
                            </div>
                        </div><!-- END REVOLUTION SLIDER -->

                        <div class="box_slider">
                          <h4 style="text-align: center !important"><b>KNOW THE HISTORY: don’t risk buying a car with hidden defects</b> </h4>
                          <ul>
                            <li>Has an accident or stolen being reported for this vehicle?</li>
                            <li>Has it been damaged by flood, fire, earth quake or other natural disasters?</li>
                            <li>Odometer discrepancies (possible rollback)?</li>
                            <li>Has the vehicle ever been recalled?</li>
                            <li>The usage of the vehicle (taxi, hire and drive, rental etc)?</li>
                            <li>Is there a risk of radioactive contamination?</li>
                      </ul>
                        </div>

                    </div>

                    <style type="text/css">
                        .horizontal-search form {
                                padding: 10px 12px;
                                background-color: #FF2929 !important;
                            }
                    </style>
                    
                    <div class="col-md-5 box_search" style="background-color: #FF2929">
                        <div class="horizontal-search v-f-p"> 
                            <div class="search-form"> 
                                <h1 class="fsearch-title hidden-xs">
                                    <i class="fa fa-search"></i><span>CHECK CAR HISTORY NOW</span>
                                </h1>

                                
                                <div class="search_widget widget">

                                  <div class="box_text">
                                    <h3>Check details of your car <b>NOW</b> <br>
                                      <b>Free</b> vehicle search</p> 
                                    </h3>

                                  </div>

                                  
                                    <form method="POST" class="register-form" id="register-form" action="{{url('/check-vehicle-web')}}">
                                        {{ csrf_field() }}
                                        
                                        <input type="text" placeholder="VIN/CHASSIS NUMBER" name="vehicle" style="font-size: 20pt !important" />

                                        <input type="submit" name="" value="Search" class="btn btn-primary btn-lg" style="background-color:  #995151; color: #FFFFFF; margin-top:20px !important;font-size: 20pt">
                                        
                                        
                                    </form>
                                    <a data-toggle="modal" data-target=".bd-example-modal-lg">
                                      <p>Where can I find VIN/ Chassis number?</p>
                                    </a>

                                    


                                    <div class="box_text">

                                    <h4 style="margin-top: 40px !important">
                                    Malaysia authorities are doing cross reference with our reports.
                                    <br>
                                     We gives you details of used imported car past history together with average market value (if available) from the origin countries. <br> </h3>

                                      <h4 style="margin-top: 40px !important">
                                       Improve your chances of buying a great car
                                     </h4>
                                       
                                      <ul>
                                         <li>Get access to comprehensive report</li>
                                         <li>Do not compromise your family safety</li>
                                         <li>Value for money</li>
                                       </ul>

                                      </div>

                                    </div>
                                     
                                </div><!-- Search Widget -->

                                

                                 
                            </div><!-- Services Sec -->

                        </div>

                        
                    </div>
    


                </div>
            </div>
        </section>





        <section class="block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="faq-sec">
                            <div class="heading4">
                                <h2>SERVICES </h2>
                                <span>Autocheck system is managed and monitored by Malaysia Automotive, Robotics & IoT Institute (MARII) endorsed within the agencies of MITI for the benefit of the automotive industry. Is a screening tools for used imported vehicle, bridging the users and data custodian origin of road transport authorities and the financial institution. This will be the official independent and trusted source to verify information and vehicle condition.</span>
                            </div>
                            <br>
                            <div class="service-circle-sec">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="service-circle">
                                            <img alt="" src="images/resource/service-cirlce1.jpg">
                                            <div class="service-simple">
                                                <i class="fa  fa-code-fork"></i>
                                                <h3>SUPPORTED</h3>
                                                <p>MALAYSIA AUTOMOTIVE, ROBOTICS & IOT INSTITUTE</p>
                                            </div>
                                        </div><!-- Service Circle -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="service-circle">
                                            <img alt="" src="images/resource/service-cirlce2.jpg">
                                            <div class="service-simple">
                                                <i class="fa  fa-android"></i>
                                                <h3>REPORT</h3>
                                                <p>DETAILED TECHNICAL DATA</p>
                                            </div>
                                        </div><!-- Service Circle -->
                                    </div>
                                    <div class="col-md-4">
                                        <div class="service-circle">
                                            <img alt="" src="images/resource/service-cirlce3.jpg">
                                            <div class="service-simple">
                                                <i class="fa  fa-html5"></i>
                                                <h3>PARTNERS </h3>
                                                <p>JAPAN, US & UK</p>
                                            </div>
                                        </div><!-- Service Circle -->
                                    </div>
                                </div>
                            </div><!-- Service Circles  -->
                        </div><!-- FAQ Sec -->
                    </div>
                </div>
            </div>
        </section>

@endsection