<style>
  .footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;
    color: #818d9e;
    text-align: center;
  }
</style>

        <div class="nav-wrapper">
            <ul class="nav flex-column">

              <!-- Dashboard  -->    
              <?php

                $buyer  = Auth::user()->akses_buyer;
                $buyer_group  = Auth::user()->akses_buyer;
                $admin  = Auth::user()->role_id;
                $verify  = Auth::user()->akses_verify;
                $kastam  = Auth::user()->akses_kastam;
                $status_admin  = Auth::user()->is_status_admin_group;
                $is_status_admin_group = Auth::user()->is_status_admin_group;

                $group_by = Auth::user()->group_by;
               ?>
              
              
              <li class="nav-item">
                <a class="nav-link {{ Request::is('dashboard') ? 'active' : '' }}" href="{{url('dashboard')}}">
                  <i class="material-icons">dashboard</i>
                  <span>Dashboard</span>
                </a>
              </li>

             
              @if($admin == 'VER')

              
              <li class="nav-item">
                <a class="nav-link {{ Request::is('sent-vehicle') ? 'active' : '' }}" href="{{url('sent-vehicle')}}">
                  <i class="material-icons">email</i>
                  <span>Sent Vehicle</span>
                </a>
              </li>
              
              <!--
              <aside class="side-nav nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">email</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sent Vehicle</a>
                    <ul class="side-nav-dropdown">
                      <li><a href="{{url('sent-vehicle')}}">Sent Vehicle (Car Dealer)</a></li>
                      <li><a href="{{url('sent-vehicle-kastam')}}">Sent Vehicle (Kastam)</a></li>
                    </ul>
                  </li>
                  
                  
                </ul>
              </aside>
              -->

              @endif


              @if($admin == 'VER')

              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">check_circle</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Sync Tasklist</a>
                    <ul class="side-nav-dropdown">
                      <li><a href="{{url('verification-tasklist')}}">Sync Tasklist (Short Report)</a></li>
                      <li><a href="{{url('verification-tasklist-kastam')}}">Sync Tasklist (Full Report)</a></li>

                      <li><a href="{{url('verification-shortreport-kastam')}}">Sync Tasklist (Short Kastam Report)</a></li>
                    </ul>
                  </li> 
                </ul>
              </aside>
              @endif

                <!-- 
              @if($admin == 'VER')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('verification-tasklist') ? 'active' : '' }}" href="{{url('verification-tasklist')}}">
                  <i class="material-icons">check_circle</i>
                  <span>Verification Tasklist</span>
                </a>
              </li>
              @endif -->

              @if($admin == 'MAN')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('verification-tasklist') ? 'active' : '' }}" href="{{url('verification-tasklist')}}">
                  <i class="material-icons">check_circle</i>
                  <span>Income</span>
                </a>
              </li>
              @endif

              
              @if($admin != 'NA' AND $admin != 'VER' AND $admin != 'US' AND $admin != 'MAN' AND $admin != 'MAR')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('search-vehicle') ? 'active' : '' }}" href="{{url('search-vehicle')}}">
                  <i class="material-icons">search</i>
                  <span>Search Vehicle</span>
                </a>
              </li>
              @endif
              <!-- End of Dashboard -->


              @if($buyer == 1)

              @endif


              @if($admin == 'NA')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-checking') ? 'active' : '' }}" href="{{url('vehicle-checking')}}">
                  <i class="material-icons">search</i>
                  <span>Vehicle Checking</span>
                </a>
              </li>
              @endif


              @if($admin == 'VER')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('open-vehicle-verify') ? 'active' : '' }}" href="{{url('open-vehicle-verify')}}">
                  <i class="material-icons">search</i>
                  <span>Open Vehicle Verify</span>
                </a>
              </li>
              @endif

              
              @if($admin == 'VER')
              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">class</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Vehicle Checked </a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a href="{{url('kastam/vehicle')}}">Vehicle Checked Kastam</a></li>
                      <li><a href="{{url('car-dealer/vehicle')}}"> Vehicle Checked Car Dealer</a></li>
                      <li><a href="{{url('buyer/vehicle')}}"> Vehicle Checked Buyer (Full Report)</a></li>
                    </ul>
                  </li>
                </ul>
              </aside>
              @endif


              
              @if($admin != 'VER')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-checked') ? 'active' : '' }}" href="{{url('vehicle-checked')}}">
                  <i class="material-icons">check_box</i>
                  <span>Vehicle Checked</span>
                </a>
              </li>
            @endif
              
  

              @if(($admin == 'NA') OR ($admin == 'KA'))
              <li class="nav-item">
                <a class="nav-link {{ Request::is('balance-history') ? 'active' : '' }}" href="{{url('balance-history')}}">
                  <i class="material-icons">history</i>
                  <span>Balance History</span>
                </a>
              </li>
              @endif

              <?php $nama_saya  = Auth::user()->id; ?>
              @if($nama_saya == '8')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('topup-balance') ? 'active' : '' }}" href="{{url('topup-balance')}}">
                  <i class="material-icons">system_update</i>
                  <span>Topup Balance </span>
                </a>
              </li>
              @endif


              @if(($admin == 'NA' OR $admin == 'KA') AND ($status_admin == '1'))
              <li class="nav-item">
                <a class="nav-link {{ Request::is('user-group') ? 'active' : '' }}" href="{{url('user-group')}}">
                  <i class="material-icons">person</i>
                  <span>User Group</span>
                </a>
              </li>
              @endif

              @if($admin == 'KA' AND $is_status_admin_group == '1') 
              <li class="nav-item">
                <a class="nav-link {{ Request::is('branch') ? 'active' : '' }}" href="{{url('branch')}}">
                  <i class="material-icons">layers</i>
                  <span>Branches</span>
                </a>
              </li>
              @endif



              @if($admin == 'AD')

              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-chart') ? 'active' : '' }}" href="{{url('vehicle-chart')}}">
                  <i class="material-icons">insert_chart</i>
                  <span>Vehicle Chart</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link {{ Request::is('invoice') ? 'active' : '' }}" href="{{url('invoice')}}">
                  <i class="material-icons">book</i>
                  <span>Invoice</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link {{ Request::is('user') ? 'active' : '' }}" href="{{url('user')}}">
                  <i class="material-icons">person</i>
                  <span>User</span>
                </a>
              </li>

              <li class="nav-item">
                <a class="nav-link {{ Request::is('balance') ? 'active' : '' }}" href="{{url('balance')}}">
                  <i class="material-icons">money</i>
                  <span>Balance</span>
                </a>
              </li>

              @endif



              @if(($admin == 'AD') OR ($admin == 'NA' AND $is_status_admin_group == "1"))

              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">settings</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Setup Parameter</a>
                    <ul class="side-nav-dropdown">
                      
                      <li><a href="{{url('brand')}}">Brand</a></li>
                      <li><a href="{{url('par-model')}}">Model</a></li>
                      <li><a href="{{url('supplier')}}">Supplier</a></li>
                      
                      @if($admin == 'AD')
                      <li><a href="{{url('role')}}">Role</a></li>
                      <li><a href="{{url('group')}}">User Group</a></li>
                      <li><a href="{{url('type-vehicle')}}">Type Vehicle</a></li>
                      <li><a href="{{url('supplier')}}">Supplier</a></li>
                      <li><a href="{{url('country-origin')}}">Country Origin</a></li>
                      <li><a href="{{url('fuel')}}">Fuel</a></li>
                      <li><a href="{{url('fee')}}">Fee</a></li>
                      <li><a href="{{url('package')}}">Package</a></li>
                      <li><a href="{{url('email')}}">Email</a></li>
                      <li><a href="{{url('branch')}}">Branch</a></li>
                      @endif
                    </ul>
                  </li>
                  
                  
                </ul>
              </aside>
              @endif



              @if(($admin == 'VER') OR ($group_by == 'NAZA') )
              <li class="nav-item">
                <a class="nav-link {{ Request::is('vehicle-past') ? 'active' : '' }}" href="{{url('vehicle-past')}}">
                  <i class="material-icons">autorenew</i>
                  <span>Vehicle Past</span>
                </a>
              </li>
              @endif

              @if(($admin == 'VER') OR ($admin == 'KA') OR ($admin == 'NA') )
              <li class="nav-item">
                <a class="nav-link {{ Request::is('not-found') ? 'active' : '' }}" href="{{url('not-found')}}">
                  <i class="material-icons">cancel</i>
                  <span>Vehicle Not Found</span>
                </a>
              </li>
              @endif

              @if($admin == 'VER')
              <li class="nav-item">
                <a class="nav-link {{ Request::is('batch-sent') ? 'active' : '' }}" href="{{url('batch-sent')}}">
                  <i class="material-icons">account_box</i>
                  <span>Batch Sent</span>
                </a>
              </li>
              @endif

              
              @if(($admin == 'MAN') OR ($admin == 'MAR') OR ($admin == 'VER'))
              
              <aside class="side-nav dropdown-toggle nav-item" id="show-side-navigation1">
                    
                <ul class="categories" style="margin-left: -15px !important; ">
                  <li><i class="material-icons">class</i>
                    <a href="#" style="color:#3d5170 !important;font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial !important; "> Report </a>
                    <ul class="side-nav-dropdown">
                      @if($admin == 'MAN' OR $admin == 'VER')
                      <li><a href="{{url('report-autocheck-management')}}">Buyer Group Report</a></li>
                      @endif

                      @if($admin != 'NA')
                      <li><a href="{{url('report-summary')}}"> Summary Report</a></li>
                      <li><a href="{{url('report-monthly')}}"> Monthly Report</a></li>
                      @endif


                      <!-- Na report -->
                      @if($admin == 'NA')

                        @if(Auth::user()->is_status_admin_group == "1")
                      <li><a href="{{url('report-vehicle')}}"> Vehicle Report</a></li>
                        @endif

                      @endif

                    </ul>
                  </li>
                  
                  
                </ul>
              </aside>

              @endif





              <!-- ============== Verify Menu ==========  -->

              <!-- ============== End Verify Menu ==========  -->

             
             <li class="nav-item">
                <a class="nav-link {{ Request::is('account') ? 'active' : '' }}" href="{{url('account')}}">
                  <i class="material-icons">account_box</i>
                  <span>Account</span>
                </a>
              </li>
              

              <li class="nav-item">
                <a class="nav-link {{ Request::is('logout') ? 'active' : '' }}" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                  <i class="material-icons">&#xE879;</i>
                  <span>Logout</span>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
              </li>

               <!-- <img src="{{asset('images/mari-logo.png')}}"> -->

            </ul>
          </div>



      <div class="footer">
        <img src="{{asset('images/mari12.png')}}">
        <p style="font-size: 10px">Powered By MAI <br>
        v3.0.0.0 [20190810]</p>
      </div>


      <footer class="main-footer">
        <ul class="nav">
            <li class="nav-item">
              <a class="nav-link" href="#">Home</a>
            </li>
          </ul>
        <img src="{{asset('images/mari2.png')}}">
      </footer>


    <script src='http://code.jquery.com/jquery-latest.js'></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
     


      <script type="text/javascript">

    $(function () {


      (function () {

        var aside = $('.side-nav'),

            showAsideBtn = $('.show-side-btn'),

            contents = $('#contents');

        showAsideBtn.on("click", function () {

          $("#" + $(this).data('show')).toggleClass('show-side-nav');

          contents.toggleClass('margin');

        });

        if ($(window).width() <= 767) {

          aside.addClass('show-side-nav');

        }
        $(window).on('resize', function () {

          if ($(window).width() > 767) {

            aside.removeClass('show-side-nav');

          }

        });

        // dropdown menu in the side nav
        var slideNavDropdown = $('.side-nav-dropdown');

        $('.side-nav .categories li').on('click', function () {

          $(this).toggleClass('opend').siblings().removeClass('opend');

          if ($(this).hasClass('opend')) {

            $(this).find('.side-nav-dropdown').slideToggle('fast');

            $(this).siblings().find('.side-nav-dropdown').slideUp('fast');

          } else {

            $(this).find('.side-nav-dropdown').slideUp('fast');

          }

        });

        $('.side-nav .close-aside').on('click', function () {

          $('#' + $(this).data('close')).addClass('show-side-nav');

          contents.removeClass('margin');

        });

      }());

      // Start chart

      

    });
      </script>