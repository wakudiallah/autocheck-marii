@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Topup Balance</h3>
              </div>
            </div>
            <!-- End Page Header -->
           


            <div class="row">

              <div class="col-lg-4 ">
              </div>
              <div class="col-lg-4 ">
              <div class="card card-small mb-2">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">

                     
                      <h5 align="center">Your Balance</h5>
                      <h3 align="center">RM 
                        @if(!empty($user_group_id->balance))
                        {{$user_group_id->balance}}
                        @endif
                      </h3>

                    </li>
                  </ul>
              </div>
              <div class="col-lg-4 ">
              </div>
            </div>

            

            </div>


            <div class="row">
              <div class="col-lg-3 ">
              </div>
              <div class="col-lg-6 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">

                      <form method="POST" class="register-form" id="register-form" action="{{url('/save-vehicle-checking')}}">

                        {{ csrf_field() }}

                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <label for="feFirstName">Top Balance</label>
                            <input type="text" class="form-control" id="feFirstName" placeholder="RM " value="" name="balance" required style="color: red; font-weight: bold">
                          </div>
                          
                        </div>

                        <div class="form-row">
                          <div class="form-group col-md-12">
                            <label for="feFirstName">Top up</label>
                            <input type="text" class="form-control" id="feFirstName" placeholder="Vehicle Identification Number" value="" name="vehicle" required>
                          </div>
                          
                        </div>


                      </form>


                      

                    </li>
                  </ul>
              </div>
              <div class="col-lg-3">
              </div>


              
            </div>
            </div>


            <div class="row">
              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">

                      
                      <table id="example" class="table table-striped table-bordered" style="width:100%">
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th>Group</th>
                                  <th>Balance</th>
                                  <th width="20%">Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php $i=1; ?>
                            
                              
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
            </div>
            


          </div>




          

            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create New User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    
                    <form method="POST" class="register-form" id="register-form" action="{{url('/save-user')}}">
                      {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Name</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Name" value="" name="name" required=""> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="feLastName">Email</label>
                                <input type="email" class="form-control" id="feLastName" placeholder="Email " value="" name="email" required=""> 
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feEmailAddress">Password</label>
                                <input type="password" class="form-control" id="feEmailAddress" placeholder="Password" value="" >
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Phone</label>
                                <input type="text" class="form-control" id="fePassword" placeholder="Phone" name="phone" min="1" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="12"> 
                              </div>
                              <div class="form-group col-md-12">
                                <label for="fePassword">Role</label>
                                <select class="form-control" name="role">
                                  <option value="" selected disabled hidden>- Select -</option>
                                  
                                </select>
                              </div>
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right">Save</button>
                          </form>

                  </div>
                  <div class="modal-footer">
                    
                  </div>
                </div>
              </div>
            </div>

          <!-- Modal -->

@endsection


@push('js')


  <<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush