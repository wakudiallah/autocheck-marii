@extends('web.layout.template2')
@section('content')

  <div class="container">
      <div class="top_content">
          <div class="row">
              <div class="col-lg-12">
                <div class="top_left_cont flipInY wow animated">
                  <h3>History Vehicle</h3>
                  
                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">#</th>
                        <th scope="col">Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Remark</th>
                        <th scope="col">By</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php $i = 1; ?>
                      <tr>
                      @foreach($data as $data)
                      <td>{{$i++}}</td>
                      <td>{{$data->created_at}}</td>
                      <td>{{$data->parameter_history_id}}</td>
                      <td>{{$data->remark}}</td>
                      <td>{{$data->historyuser->name}}</td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>


@endsection