@extends('admin.layout.template')

@section('content')

      <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

        <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title"> Vehicle Report </h3>
              </div>
            </div>
            <!-- End Page Header -->
           

            <div class="row" style="margin-top: 80px !important">
              <div class="col-md-2 mb-4"></div>

              <div class="col-md-8 mb-4">
                <div class="card card-small mb-4">
                    <div class="card-header border-bottom">
                      <h6 class="m-0"> Vehicle Report</h6>
                    </div>

                    <ul class="list-group list-group-flush">
                      <li class="list-group-item px-3">
                        <form method="POST" class="" id="register-form" action="report-vehicle-na-view">
                          {{ csrf_field() }}

                        <?php $month = date("m/d/Y", strtotime($date)); ?>

                        <div class="form-row">
                          <div class="form-group col-md-6">
                            <label for="feFirstName">Month</label>
                            <input type="text" class="form-control" id="datepicker" placeholder="Month" value="{{$month}}" name="month" required>
                          </div>

                          <div class="form-group col-md-6">
                            <label for="feFirstName">Supplier</label>
                            <select name="supplier" class="form-control select2" required  data-show-subtext="true" data-live-search="true">
                              
                              <option value="All">All</option>
                              @foreach($data_supplier as $data1)
                              <option value="{{$data1->id}}" {{ $supplier == $data1->id ? 'selected' : '' }}>{{$data1->supplier}}</option>

                              @endforeach
                            </select>
                          </div>
                          
                        </div>

                        <button type="submit" class="btn btn-accent" style="float: right">Submit</button>

                        </form>

                      </li>
                    </ul>
                </div>
              </div>

              <div class="col-md-2 mb-4"></div>

            </div>


            <div class="row">

              


              <div class="col-md-12 mb-4">
                <div class="card card-small mb-4">

                  
                  
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item px-3">
                      
                      <form method="POST" class="" id="register-form" action="report-vehicle-na-print">
                        {{ csrf_field() }}

                        <input type="hidden" class="form-control" value="{{$date}}" name="month" >   
                        <input type="hidden" class="form-control" value="{{$supplier}}" name="supplier" >    

                        <button type="submit" class="btn btn-accent" style="float: right;">Export</button>

                    </form>

                      <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >
                          <thead>
                              <tr>
                                  <th width="5%">No</th>
                                  <th width="20%">Vehicle</th>
                                  <th width="20%">Status</th>
                                  <th width="20%">Date Verify</th>
                                  <th width="20%">Supplier</th>
                              </tr>
                          </thead>
                          <tbody>
                            <?php 
                              $i = 1;
                            ?>

                            @foreach($vin as $data)

                            <tr>
                              <td>{{$i++}}</td>
                              <td>{{$data->vehicle}}</td>
                              <td>
                                @if($data->status == "40")
                                  Complete
                                @elseif($data->status == "40")
                                  Reject
                                @endif
                              </td>
                              <td>{{date("d/m/Y", strtotime($data->status_vehicle->updated_at))}}</td>
                              <td>
                                {{$data->supplier->supplier}}
                              </td>
                            </tr>
                            
                            @endforeach
                          </tbody>
                          
                      </table>

                    </li>
                  </ul>
              </div>
              </div>
            </div>

          </div>

          

            

@endsection


@push('js')

  
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
  <script>
  $(function() {
     $( "#datepicker" ).datepicker();
   });
  $(function() {
     $( "#datepicker2" ).datepicker();
   });
  </script>



  <<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

<script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush