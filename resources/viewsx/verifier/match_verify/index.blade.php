@extends('admin.layout.template')

@section('content')

  <div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Vehicle Match</h3>
              </div>
            </div>
            <!-- End Page Header -->
            <!-- Default Light Table -->
            <div class="row">
              <div class="col-lg-6">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Vehicle From ({{$data->request_by->group_by}})</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col border-bottom">

                          <form method="POST" class="register-form" id="register-form" action="{{url('/save-vehicle-checking')}}">

                            {{ csrf_field() }}

                           

                            <div class="form-row">
                              <div class="form-group col-md-5">
                                <input type="text" class="form-control is-valid" id="validationServer01" placeholder="First name" value="Catalin" required>
                                <div class="valid-feedback">The first name looks good!</div>
                              </div>
                              <div class="form-group col-md-5">
                                <input type="text" class="form-control is-valid" id="validationServer02" placeholder="Last name" value="Vasile" required>
                                <div class="valid-feedback">The last name looks good!</div>
                              </div>
                              <div class="form-group col-md-2">
                                <!-- Checkboxes -->
                          
                                <div class="custom-control custom-checkbox mb-1">
                                  <input type="checkbox" class="custom-control-input" id="formsCheckboxDefault">
                                  <label class="custom-control-label" for="formsCheckboxDefault">Match</label>
                                </div>
                            
                              </div>
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Vehicle Identification Number</label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Vehicle Identification Number" value="" name="vehicle" required style="color: red">
                              </div>
                              
                            </div>
                            
                            
                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Country Origin</label>
                                 <select id="feInputState" name="country" class="form-control" required>
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  
                                </select>
                              </div>
                            </div>

                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Make / Brand</label>
                                 
                                  <input type="text" class="form-control" id="feFirstName" placeholder="Make / Brand" value="{{$vehicle_api->brand}}" name="brand" >
                                  
                              </div>
                            </div>

                            <div class="form-row">
                              
                              <div class="form-group col-md-12">
                                <label for="fePassword">Model</label>
                                 <input type="text" class="form-control" id="feFirstName" placeholder="Make / Brand" value="{{$vehicle_api->model}}" name="model" >
                              </div>
                            </div>

                            <div class="form-group">
                              <label for="feInputAddress">Engine Number / Engine Model</label>
                              <input type="text" class="form-control" id="feInputAddress" placeholder="Engine Number" placeholder="Engine Number" name="engine_number" value="{{$vehicle_api->engine_number}}"> 
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-5">
                                <label for="feInputCity">Engine Capacity (cc)</label>
                                <input type="text" class="form-control" placeholder="Engine Capacity (cc)" value="{{$data->cc}}"> 
                              </div>
                              <div class="form-group col-md-5">
                                <label for="feInputCity">Vehicle Registered Date</label>
                                <input type="text" class="form-control" id="datepicker" name="registered_date" placeholder="Vehicle Registered Date" required> 
                              </div>
                              <div class="form-group col-md-2">
                                <label for="feInputCity">Vehicle</label>
                                <input type="text" class="form-control" id="datepicker" name="registered_date" placeholder="Vehicle Registered Date" required> 
                              </div>

                            </div>

                            <div class="form-group">
                              <label for="feInputAddress">Fuel</label>
                              <select id="feInputState" name="fuel" class="form-control" required>
                                  <option value="" selected disabled hidden>- Please Select -</option>
                                  
                                </select>
                            </div>



                            <button type="submit" class="btn btn-accent">Submit</button>
                          </form>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Vehicle API ({{$data->status_vehicle->api_from}})</h6>
                  </div>
                  <ul class="list-group list-group-flush">
                    <li class="list-group-item p-3">
                      <div class="row">
                        <div class="col">

                    
                              <form method="POST" class="register-form" id="register-form" action="{{url('/update-vehicle-checking/'.$data->id)}}">
                                    {{ csrf_field() }}
                                  

                                  <div class="form-row">
                                    <div class="form-group col-md-12">
                                      <label for="feDescription">Vehicle Identification Number</label>
                                      <input type="text" class="form-control" name="vehicle_edit" placeholder="Vehicle Identification Number" value="{{$data->vehicle}}" required=""> 
                                    </div>

                                   <div class="form-group col-md-12">
                                      <label for="feDescription">Supplier</label>
                                      <select id="feInputState" name="supplier_edit" class="form-control">
                                        
                                        @foreach($supplier as $data)
                                        
                                         <option value="{{ $data->id }}" {{ $data->supplier_id == $data->id ? 'selected' : '' }}>{{$data->supplier}}</option>

                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="form-group col-md-12">
                                      <label for="feDescription">Type</label>
                                      <select name="type_edit" class="form-control">
                                        
                                        @foreach($type as $data)
                                        
                                         <option value="{{ $data->id }}" {{ $data->type_id == $data->id ? 'selected' : '' }}>{{$data->type_vehicle}}</option>

                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="form-group col-md-12">
                                      <label for="feDescription">Country Origin</label>
                                      <select id="feInputState" name="country_edit" class="form-control">
                                        
                                        @foreach($country as $data)
                                        
                                         <option value="{{ $data->id }}" {{ $data->country_origin_id == $data->id ? 'selected' : '' }}>{{$data->country_origin}}</option>

                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="form-group col-md-12">
                                      <label for="feDescription">Make / Brand</label>
                                      <select id="brand_value_edit" name="brand_edit" class="form-control">
                                        <option value="" selected disabled hidden>- Please Select -</option>

                                        @foreach($brand as $data)
                                        
                                         <option value="{{ $data->id }}" {{ $data->brand_id == $data->id ? 'selected' : '' }}>{{$data->brand}}</option>

                                        @endforeach
                                      </select>
                                    </div>

                                    <div class="form-group col-md-12">
                                      <label for="feDescription">Model</label>
                                      <select id="model_value_edit" name="model_edit" class="form-control">
                                        <option value="" selected disabled hidden>- Please Select -</option>
                                        
                                        @foreach($model as $data)
                                        
                                         <option value="{{ $data->id }}|{{$data->brand_id}}" {{ $data->model_id == $data->id ? 'selected' : '' }}>{{$data->model}}</option>

                                        @endforeach
                                      </select> 
                                    </div>

                                    <div class="form-group col-md-12">
                                        <label for="feInputAddress">Engine Number</label>
                                        <input type="text" class="form-control"  placeholder="Engine Number" name="engine_number_edit" value="{{$data->engine_number}}"> 
                                    </div>

                                          <div class="form-group col-md-6">
                                            <label for="feInputCity">Engine Capacity (cc)</label>
                                            <input type="text" class="form-control" placeholder="Engine Capacity (cc)" oninput="this.value=this.value.replace(/[^0-9]/g,'');" maxlength="10" name="engine_capacity_edit" value="{{$data->cc}}"> 
                                          </div>

                                          <?php  $tarikh =  date('m/d/Y ', strtotime($data->vehicle_registered_date)); ?>

                                          <div class="form-group col-md-6">
                                            <label for="feInputCity">Vehicle Registered Date Edit</label>
                                            <input type="text" class="form-control" id="datepicke2r" name="registered_date_edit" placeholder="Vehicle Registered Date" value="{{$tarikh}}"> 
                                          </div>


                                      <div class="form-group col-md-12">
                                        <label for="feDescription">Fuel{{$data->fuel_id}}</label>
                                        <select id="feInputState" name="fuel_edit" class="form-control">
                                          
                                          @foreach($fuel as $data)
                                          
                                           <option value="{{ $data->id }}" {{ $data->fuel_id == $data->id ? 'selected' : '' }}>{{$data->fuel}}</option>

                                          @endforeach
                                        </select>
                                      </div>

                                  


                                   
                            </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>              
             
            </div>
            <!-- End Default Light Table -->
          </div>




@endsection

@push('js')

  

  <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value').children().clone();
      
        $('#brand_value').change(function() {
          $('#model_value').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value').append(this);
                 }
            });
          $('#model_value').val('');
        });
    });
    </script>


    <script type="text/javascript">
       $( document ).ready(function() {
       var options = $('#model_value_edit').children().clone();
      
        $('#brand_value_edit').change(function() {
          $('#model_value_edit').children().remove();
        var rawValue =this.value;
         options.each(function () {
                var newValue = $(this).val().split('|');
                if (rawValue == newValue[1] ) {
                    $('#model_value_edit').append(this);
                 }
            });
          $('#model_value_edit').val('');
        });
    });
    </script>


<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script>
    $(function() {
       $( "#datepicker" ).datepicker();
     });
    $(function() {
       $( "#datepicker2" ).datepicker();
     });
    </script>

   
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

    <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="dataTables.bootstrap.js"></script>

  <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable();
      } );
  </script>

@endpush