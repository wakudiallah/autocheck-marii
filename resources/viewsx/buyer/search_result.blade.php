@extends('admin.layout.template')

@section('content')

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Search</span>
        <h3 class="page-title">Search</h3>
      </div>
    </div>
    <!-- End Page Header -->
    
    
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 mb-4">
        <div class="card card-small card-post h-100">
          <div class="card-post__image" style="background-image: url('images/content-management/7.jpeg');"></div>
          <div class="card-body">
            <h5 class="card-title">
              <a class="text-fiord-blue" href="#">CONGRATULATIONS ! YOUR CAR RECORDS FOUND</a>
            </h5>

            <form method="POST" class="register-form" id="register-form" action="{{url('/pay-chassis')}}">

                {{ csrf_field() }}

            <div class="row">

              <div class="col-md-6">
                <p>Chasis Number</p>
                <input type="text" name="carid" class="form-control input-text" id="name" placeholder="Chasis Number" value="{{$search->cars[0]->chassisNumber}}" style="color: black !important" readonly="" />
              </div>
              <div class="col-md-6">
                <p>Brand</p>
                <input type="text" name="make" class="form-control input-text" id="name" placeholder="Chasis Number" value="{{$search->cars[0]->make}}" style="color: black !important" readonly="" />
              </div>

              <div class="col-md-6">
                <p>Model</p>
                <input type="text" name="model" class="form-control input-text" id="name" placeholder="Chasis Number" value="{{$search->cars[0]->model}}" style="color: black !important" readonly="" />
              </div>
              <div class="col-md-6">
                <p>Engine Model</p>
                <input type="text" name="engine" class="form-control input-text" id="name" placeholder="Chasis Number" value="{{$search->cars[0]->engine}}" style="color: black !important" readonly="" />
              </div>
            
            </div>

            
            
              

          </div>
          <div class="card-footer text-muted border-top py-3">
            <span class="d-inline-block">
                <div class="row">
                    <div class="col-md-12">
                      

                      <button type="submit" class="btn input-btn btn-md btn-success" style="float: right; margin-right: 30px">Buy</button>
                      
                      <a  class="btn btn-md btn-warning" id="showmenu">
                        Other Chassis <i class="icon-doc"></i></a>
                    
                </div>
                </div>
            </span>
          </div>

          </form>
        </div>
      </div>



    </div>


    <div class="row menu" style="display: none;">
        <div class="col-lg-12 mb-4">
          
          <!-- Input & Button Groups -->
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">Search</h6>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item px-3">
                
                
                <form id="search" action="{{url('check-vehicle-buyer')}}" method="post" enctype="multipart/form-data">

                  {{ csrf_field() }}

                  <div class="input-group mb-5">
                    <input type="text" class="form-control form-control-lg" placeholder="Vehicles" aria-label="Recipient's username" aria-describedby="basic-addon2" name="Vehicle" required="">
                    <div class="input-group-append">
                      <input type="Submit" class="btn btn-white" value="Search">
                    </div>
                  </div>
                  
                </form>
              </li>
              
                
            </ul>

          </div>
          <!-- / Input & Button Groups -->
        </div>

        
      </div>
    

  </div>


@endsection

@push('js')
    
    <script type="text/javascript">
        $(document).ready(function() {
            $('#showmenu').click(function() {
                    $('.menu').toggle("slide");
            });
        });
    </script>

@endpush