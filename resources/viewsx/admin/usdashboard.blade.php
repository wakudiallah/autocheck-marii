@extends('admin.layout.template')

@section('content')

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    
    


    <!-- Small Stats Blocks -->
    <div class="row">
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Vehicle</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_verified }}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle_pending}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>

    </div>

    <div class="row">
        <div class="col-lg-12 mb-4">
          
          <!-- Input & Button Groups -->
          <div class="card card-small mb-4">
            <div class="card-header border-bottom">
              <h6 class="m-0">Search</h6>
            </div>
            <ul class="list-group list-group-flush" style="margin-left: 90px !important; margin-right: 90px !important; margin-top: 40px !important">
              <li class="list-group-item px-3">
                
                
                <form id="search" action="{{url('check-vehicle-buyer')}}" method="post" enctype="multipart/form-data">

                  {{ csrf_field() }}

                  <div class="input-group mb-5">
                    <input type="text" class="form-control form-control-lg" placeholder="Vehicles" name="vehicle" required style="color:red">
                    <div class="input-group-append">
                      <input type="Submit" class="btn btn-success" value="Search" >
                    </div>
                  </div>
                  
                </form>
              </li>
              
                
            </ul>

          </div>
          <!-- / Input & Button Groups -->
        </div>

        
      </div>
    

  </div>


@endsection