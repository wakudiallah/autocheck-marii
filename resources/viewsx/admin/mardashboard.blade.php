@extends('admin.layout.template_dashboard')

@section('content')

  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>

<div class="main-content-container container-fluid px-4">
    <!-- Page Header -->
    <div class="page-header row no-gutters py-4">
      <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
        <span class="text-uppercase page-subtitle">Dashboard</span>
        <h3 class="page-title">Dashboard</h3>
      </div>
    </div>
    <!-- End Page Header -->
    <!-- Small Stats Blocks -->
    <div class="row">
      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Total Vehicles</span>
                <h6 class="stats-small__value count my-3">{{$total_vehicle}}</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-2"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Verified Vehicles</span>
                <h6 class="stats-small__value count my-3">0</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--decrease"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-3"></canvas>
          </div>
        </div>
      </div>
      <div class="col-lg col-md-4 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <span class="stats-small__label text-uppercase">Pending Verifications</span>
                <h6 class="stats-small__value count my-3">0</h6>
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-4"></canvas>
          </div>
        </div>
      </div>

      
      <div class="col-lg col-md-6 col-sm-6 mb-4">
        <div class="stats-small stats-small--1 card card-small">
          <div class="card-body p-0 d-flex">
            <div class="d-flex flex-column m-auto">
              <div class="stats-small__data text-center">
                <a class="mb-2  mr-1" data-toggle="modal" data-target=".bd-example-modal-sm1" style="cursor: pointer">

                  <span class="stats-small__label text-uppercase">Total Group</span>
                

                  <h6 class="stats-small__value count my-3">{{$total_group}}</h6>
                </a>   
                                                    
              </div>
              <div class="stats-small__data">
                <span class="stats-small__percentage stats-small__percentage--increase"></span>
              </div>
            </div>
            <canvas height="120" class="blog-overview-stats-small-1"></canvas>
          </div>
        </div>
      </div>

      
      
    </div>


    <!-- Chart -->
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Vehicle Chart</h6>
          </div>
          <div class="card-body pt-0">

              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
          </div>
        </div>
      </div>


      <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">Income Chart</h6>
          </div>
          <div class="card-body pt-0">
              <div id="container_line" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
              
          </div>
        </div>
      </div>


    </div>


    <!-- End Chart -->

    <!-- End Small Stats Blocks -->
    <div class="row">
      <!-- Users Stats -->
      
      <div class="col-lg-7 col-md-12 col-sm-12 mb-4">
        <div class="card card-small">
          <div class="card-header border-bottom">
            <h6 class="m-0">User Group Chart</h6>
          </div>
          <div class="card-body pt-0">

              <canvas id="myChart" style="min-width: 310px; height: 400px; margin: 0 auto"></canvas>

          </div>
        </div>
      </div>

      <!-- End Users Stats -->
      <!-- Users By Device Stats -->
      <div class="col-lg-5 col-md-6 col-sm-12 mb-4">
        <div class="card card-small h-100">
          <div class="card-header border-bottom">
            <h6 class="m-0">Member Activity</h6>
          </div>
          <div class="card-body d-flex py-0 table-responsive">
            <table class="table table-striped table-bordered table-responsive" id="example" style="font-size: 11px !important">
                  <thead>
                    <tr>
                      <th>Member</th>
                      <th>Activity</th>
                      <th>Date Activity</th>
                      <th>Case</th>
                      <th>St Verify</th>
                      <th>History</th>
                    </tr>
                    
                  </thead>

                  <tbody>
                    @foreach($data as $data)
                    <tr>
                      <td>{{$data->name}}</td>
                      <td>
                        @if(!empty($data->history->parameter_history_id))
                        {{$data->history->parameter_history_id}}
                        @endif
                      </td>
                      <td>{{$data->last_login}}</td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
          </div>
          <div class="card-footer border-top">
            <div class="row">
              
              <div class="col">
                
              </div>
              <div class="col text-right view-report">
                <a href="#">Full report &rarr;</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- End Users By Device Stats -->
      
    </div>

    <!-- Modal User Group -->
     @foreach($data_group as $data)
      <div class="modal fade bd-example-modal-sm1" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">User Group </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                
            <table id="example" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%" >  
              <thead>
                <tr>
                    <th width="5%">No</th>
                    <th>Gorup</th>
                    <th>Total User</th>
                </tr>
              </thead>
              <tbody>
              <?php $i=1; ?>
              @foreach($data_group as $data)
              <tr>
                <td>{{$i++}}</td>
                <td>{{$data->group_name}}</td>
                <td>{{$data->total_group_user->count()}}</td>
              </tr>
              @endforeach

              </tbody> 
            </table> 
            </div>
            <div class="modal-footer">
              
              
            </div>


          </div>
        </div>
      </div>
      @endforeach
    <!-- end Modal User Group -->


  </div>


@endsection


@push('js')

  <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">

  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
  
   <script type="text/javascript">
            
            $(document).ready(function() {
                $('#example').DataTable();
            } );

  </script>

  <script type="text/javascript">
    $(document).ready(function () {
    $('#dtBasicExample').DataTable();
    $('.dataTables_length').addClass('bs-select');
    });
  </script>


   <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>

  <script type="text/javascript">

    $(function () { 
        var data_vehicle = <?php echo $list_vehicle; ?>;
        var data_api = <?php echo $list_api; ?>;
        var data_not_api = <?php echo $list_not_api; ?>;


        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Vehicle'
            },
            subtitle: {
                text: (new Date()).getFullYear()
            },
            xAxis: {
                categories: [
                    'Jan',
                    'Feb',
                    'Mar',
                    'Apr',
                    'May',
                    'Jun',
                    'Jul',
                    'Aug',
                    'Sep',
                    'Oct',
                    'Nov',
                    'Dec'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Vehicle (vehicle)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} app</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },

            series: [{
                name: 'Total Vehicle',
                data: data_vehicle
                

            }, {
                name: 'Total By API',
                data: data_api

            

            }, {
                name: 'Total By Manual',
                data: data_not_api

            }]
        });

        });
</script>


<!-- Pie Diagram -->
<script type="text/javascript">

  var list_name_group_user = <?php echo $list_name_group_user; ?>;
  var list_name_group_total = <?php echo $list_name_group_total; ?>;

  var ctx = document.getElementById("myChart").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: list_name_group_user,
      datasets: [{
        backgroundColor: [
          "#2ecc71",
          "#e74c3c",
          "#f1c40f",
          "#3498db",
          "#95a5a6",
          "#9b59b6",
          "#34495e"
        ],
        data: list_name_group_total
      }]
    }
  });
</script>
<!-- end pie diagram -->

<!-- Line chart -->
<script type="text/javascript">

  var list_line_chart = <?php echo $list_line_chart; ?>;
  

  Highcharts.chart('container_line', {
    chart: {
        marginBottom: 80
    },
    title: {
                text: 'Income '
            },

    xAxis: {
        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        labels: {
            rotation: 90
        }
    },

      series: [{

          name: 'Naza',
          data: list_line_chart
      },
      {
          name: 'Kastam',
          data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
      }]
      
  });
</script>
<!-- End Chart -->





<script type="text/javascript">
  Highcharts.chart('containerx', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
    },
    title: {
        text: 'Browser market shares in January, 2018'
    },
    tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
        pie: {
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b>: {point.percentage:.1f} %'
            }
        }
    },
    series: [{
        name: 'Brands',
        colorByPoint: true,
        data: [{
            name: 'Chrome',
            y: 61.41
        }, {
            name: 'Internet Explorer',
            y: 11.84
        }, {
            name: 'Firefox',
            y: 10.85
        }, {
            name: 'Edge',
            y: 4.67
        }, {
            name: 'Safari',
            y: 4.18
        }, {
            name: 'Sogou Explorer',
            y: 1.64
        }, {
            name: 'Opera',
            y: 1.6
        }, {
            name: 'QQ',
            y: 1.2
        }, {
            name: 'Other',
            y: 2.61
        }]
    }]
});
</script>

@endpush