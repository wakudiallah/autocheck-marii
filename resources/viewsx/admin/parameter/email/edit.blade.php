@extends('admin.layout.template_dashboard')

@section('content')
	
		<div class="main-content-container container-fluid px-4">
            <!-- Page Header -->
            <div class="page-header row no-gutters py-4 mb-3 border-bottom">
              <div class="col-12 col-sm-4 text-center text-sm-left mb-0">
                <h3 class="page-title">Email</h3>
              </div>
            </div>
            <!-- End Page Header -->

            <div class="row">

              <div class="col-lg-12 mb-4">
              <div class="card card-small mb-4">
                  <div class="card-header border-bottom">
                    <h6 class="m-0">Edit Email</h6>
                  </div>
                
	                <ul class="list-group list-group-flush">
	                    <li class="list-group-item px-3">
	                      
	                    	<form method="POST" class="register-form" id="register-form" action="{{url('update-email/'.$data->id)}}" >

                         {{ csrf_field() }}

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Email </label>
                                <input type="email" class="form-control" id="feFirstName" placeholder="Email" value="{{$data->email}}" name="email" required=""> 
                              </div>
                              
                            </div>

                            <div class="form-row">
                              <div class="form-group col-md-12">
                                <label for="feFirstName">Desc </label>
                                <input type="text" class="form-control" id="feFirstName" placeholder="Desc" value="{{$data->desc}}" name="desc" required=""> 
                              </div>
                              
                            </div>
                            
                            <button type="submit" class="btn btn-accent" style="float: right;">Save</button>
                        </form>


	                    </li>
	                </ul>
	              </div>
            </div>
        </div>
   </div>
	

@endsection