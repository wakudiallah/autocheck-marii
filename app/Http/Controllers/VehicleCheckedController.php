<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use App\Model\VehicleApi;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;

class VehicleCheckedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $role  = Auth::user()->role_id;
        $group = Auth::user()->group_by;
        $me    = Auth::user()->id;
        $company_name = Auth::user()->user_group_id;

        
        if($role == 'VER'){ //verify
            $data = VehicleChecking::whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            return view('verifier.vehicle_checked.index', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }


        elseif($role == 'NA'){


            $data = VehicleChecking::where('company_name', $company_name)->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('company_name', $company_name)->orderBy('id', 'DESC')->get();

            foreach ($data2 as $p) {
                
                $vehicle_manual = VehicleManual::where('id_vehicle', $p->id_vehicle)->first();
            }

            $data2 = VehicleChecking::where('company_name', $company_name)->orderBy('id', 'DESC')->get();

             

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            return view('share.vehicle_checked.index', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3', 'vehicle_manual'));
        }


        elseif($role == 'KA'){

            $ka_branch =  Auth::user()->is_role_kastam;
            $user =  Auth::user()->id;
            

            $data = VehicleChecking::where('group_by', $role)->where('company_name', $ka_branch)->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('group_by', $role)->where('company_name', $ka_branch)->orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::where('group_by', $role)->where('company_name', $ka_branch)->orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('share.vehicle_checked.index', compact('data', 'data2', 'data3','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model'));
        }

        elseif($role == 'MAR'){
            $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();
            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('company_name', $company_name)->orderBy('id', 'DESC')->get();

            foreach ($data2 as $p) {
                
                $vehicle_manual = VehicleManual::where('id_vehicle', $p->id_vehicle)->first();
            }

            return view('share.vehicle_checked.index', compact('data', 'data2', 'data3','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model'));
        }

        elseif($role == 'US'){


            $data = VehicleChecking::where('created_by', $me)->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('created_by', $me)->orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::where('created_by', $me)->orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('share.vehicle_checked.index', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }
        elseif($role == 'AD'){


            $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('admin.vehicle_checked.vehicle_checked', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }

       elseif($role == 'MAN'){


            $data = VehicleChecking::orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::orderBy('id', 'DESC')->get();
            $data3 = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::where('status', 1)->get();
            $model    =  ParameterModel::where('status', 1)->get();

            return view('admin.vehicle_checked.vehicle_checked', compact('data', 'data2','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }
             
        
    }


    public function open_vehicle()
     {
        $role  = Auth::user()->role_id;
        $group = Auth::user()->group_by;
        $me    = Auth::user()->id;

        

        
        if($role == 'VER'){ //verify
            $data = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->get();
            $data2 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->get();

            $data3 = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->get();

            $verify_manual = VehicleChecking::whereIn('status', ['25','10', '20'])->orderBy('id', 'DESC')->where('is_sent', '1')->get();
            

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            

            return view('verifier.open_vehicle.index', compact('verify_manual','data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        }

     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tasklist()
    {
        
        
        $data = VehicleChecking::whereNotIn('status', ['25','30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('searching_by', 'API')->get();
        $data2 = VehicleChecking::whereNotIn('status', ['25','30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('searching_by', 'API')->get();

        $data3 = VehicleChecking::whereNotIn('status', ['25','30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('group_by', 'NA')->where('searching_by', 'API')->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.tasklist.index', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }


    public function tasklist_kastam()
    {
        
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();
        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['US'])->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        return view('verifier.tasklist.index_kastam', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function tasklist_kastam_shortreport()
    {
        $data = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->get();

        $data2 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->get();

        $data3 = VehicleChecking::whereNotIn('status', ['30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->whereIn('group_by', ['KA'])->get();

        $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

        $vehicle_api  = VehicleApi::get();

        $country  =  ParameterCountryOrigin::where('status', 1)->get();
        $fuel     =  ParameterFuel::where('status', 1)->get();
        $supplier =  ParameterSupplier::where('status', 1)->get();
        $type     =  ParameterTypeVehicle::where('status', 1)->get();
        $brand    =  Brand::get();
        $model    =  ParameterModel::get();

        
        return view('verifier.tasklist.short_report_kastam', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }

    
    public function kastam_vehicle_complete()
    {
            
            $data = VehicleChecking::where('group_by', 'KA')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();
            $data2 = VehicleChecking::where('group_by', 'KA')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data3 = VehicleChecking::where('group_by', 'KA')->whereIn('status', ['30', '40'])->orderBy('id', 'DESC')->get();

            $data_verify = VehicleChecking::orderBy('id', 'DESC')->get();

            $vehicle_api  = VehicleApi::get();

            

            $country  =  ParameterCountryOrigin::where('status', 1)->get();
            $fuel     =  ParameterFuel::where('status', 1)->get();
            $supplier =  ParameterSupplier::where('status', 1)->get();
            $type     =  ParameterTypeVehicle::where('status', 1)->get();
            $brand    =  Brand::get();
            $model    =  ParameterModel::get();

            return view('verifier.vehicle_checked.kastam_vehicle_complete', compact('data', 'data2','data_verify','vehicle_api', 'country', 'fuel', 'supplier', 'type', 'brand', 'model', 'data3'));
    }



    public function car_dealer_vehicle_complete()
    {
        
    }

    public function buyer_vehicle_complete()
    {
        
    }



}
