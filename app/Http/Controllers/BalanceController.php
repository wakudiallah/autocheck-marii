<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use App\Model\HistoryBalance;
use App\Model\UserGroup;
use Response;
use App\User;

class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         $data = UserGroup::get();
         $data2 = UserGroup::get();

        return view('share.balance.index', compact('data', 'data2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function topup()
    {
        $user_id             = Auth::user()->user_group_id;

        $user_group_id         = UserGroup::where('user_id', $user_id)->first();

        

        return view('share.balance.topup', compact('user_group_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $user             = Auth::user()->id;
        $role_me  = Auth::user()->user_group_id;
        $branch_me             = Auth::user()->is_role_kastam;


        $last_balance = UserGroup::where('id',$id)->first();
        $last_balance_me = $last_balance->balance;
        $get_role_user =  $last_balance->user_id;

        $balance =  $request->input('balance');
        $total_balance = $balance + $last_balance_me;

        

        $data = UserGroup::where('id',$id)->update(array('balance' => $total_balance ));

        

        $data             =  new HistoryBalance;

        $data->balance    = $total_balance;
        $data->debit    = $balance;
        $data->transaction_fee       = '0';
        $data->desc       = 'Top-up Balance';
        $data->created_by =  $user;
        if(empty($last_balance->branch)){
        $data->user_id     =  $get_role_user;
        }
        
        else{
        $data->user_id     =  $last_balance->branch;
        }
        
        
        $data->save();
        

        return redirect('balance')->with(['success' => 'Balance data successfully updated']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function history($id)
    {
         $data = HistoryBalance::where('user_id', $id)->get();

        return view('share.balance_history.index', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
