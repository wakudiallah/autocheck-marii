<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\ParameterModel;
use App\Model\Brand;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Auth;
use Mail;
use Session;

class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data = ParameterModel::orderBy('id', 'DESC')->get();
        $brand = Brand::get();
        
        return view('admin.parameter.par_model.index', compact('data', 'brand'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user             = Auth::user()->id;

        $data             =  new ParameterModel;
        
        $data->model      = $request->model;
        $data->brand_id   = $request->brand;
        $data->created_by =  $user;
        $data->status     =  '1';
        
        $data->save();


        return redirect('/par-model')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ParameterModel::where('id',$id)->first();
        $brand = Brand::where('status', 1)->get();

        return view('admin.parameter.par_model.edit', compact('data', 'brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user             = Auth::user()->id;

        $model =  $request->input('model');
        $brand =  $request->input('brand');

        $data = ParameterModel::where('id',$id)->update(array('model' => $model, 'brand_id' => $brand, 'updated_by' => $user));

         return redirect('par-model')->with(['success' => 'Data successfully updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ParameterModel::where('id',$id)->delete();   
        
        return redirect('pra-model')->with(['success' => 'Data successfully deleted']);
    }
}
