<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\AntiAttrition;
use App\User;
use App\Transformers\VehicleTransformer;
use Auth;
use App\Model\SettlementInfo;
use App\Model\VehicleChecking;
use Debugger;
use Illuminate\Support\Facades\Log;
use App\Transformers\MomTransformer;
use Response;

class ApiSearchController extends Controller
{
    public function search_vehicle_marii(Request $request,VehicleChecking $mo){
        $this->validate($request, [
            'vehicle'                 =>'required'
        ]);
       // $auth = Auth::user();
        
        //$mo = $mo->find($request->id_mo);
        $mo = VehicleChecking::where('vehicle', $request->vehicle)->first();

        $count = VehicleChecking::where('vehicle', $request->vehicle)->count();

        if($count=='1'){
            return fractal()
                ->item($mo)
                ->transformWith(new VehicleTransformer)
                ->includePosts()
                ->toArray();
        }
        elseif($count>'1'){
            return Response::json(["response" => "Duplicated, Please Contact Admin"]);
        }
        else{
            return Response::json(["response" => "Not Found"]);
        }
    }



}
