<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use App\Authorizable;
use App\Model\UserGroup;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;
use Redirect;
use App\Model\ParameterBranchKastam;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * 
     */
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $role  = Auth::user()->role_id;

        if($role == "AD")
        {
            $data = User::orderBy('id', 'DESC')->get();
            $role = Role::get();
            $group = UserGroup::get();

            return view('admin.user.index', compact('data', 'role', 'group'));

        }
        else{

            return Redirect::back()->withErrors(['warning', 'Not Found']);
        }
    }

    public function index_group()
    {
        $role  = Auth::user()->user_group_id;
        $status_admin  = Auth::user()->is_status_admin_group;



        if($status_admin == '1'){
            $data = User::where('user_group_id', $role)->orderBy('id', 'DESC')->get();

            $data_detail = User::where('user_group_id', $role)->orderBy('id', 'DESC')->get();
            
            $role = Role::get();
            $group = UserGroup::get();
            $branch = ParameterBranchKastam::get();

            return view('admin.user_group.index', compact('data','data_detail', 'role', 'group', 'branch'));
       
        }else{

            return redirect('/dashboard')->with(['warning' => 'Sory, Dont have access']);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.user.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store_group(Request $request)
    {
        $id_user = Auth::user()->id;
        $role  = Auth::user()->role_id;
        $user_group_id = Auth::user()->user_group_id;

        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $pass = bcrypt($password);


        $check_email = User::where('email', $email)->count();

        if($check_email >= '1'){

            return redirect('/user-group')->with(['warning' => 'Email  already exist']);

        } 

        $data             =  new User;

        $password =  $request->input('password');
        $passx     = bcrypt($password);
             
         $data->name       = $request->name;
         $data->email      = $request->email;
         $data->password   = $passx;
         $data->role_id    =  $role;
         $data->is_role_kastam     = $request->branch;
         $data->phone      =  $request->phone;
         $data->status     =  '1';
         $data->created_by =  $id_user;
         $data->user_group_id =  $user_group_id;
         $data->group_by =  $user_group_id;

        
        $data->save();

        return redirect('/user-group')->with(['success' => 'Data saved successfuly']);



    }


    public function store(Request $request)
    {
        $id_user = Auth::user()->id;

        $name = $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->phone;
        $role = $request->role;
        $pass = bcrypt($password);

        $check_email = User::where('email', $email)->count();
        $get_email = User::where('email', $email)->first();

        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        Mail::send('web.register_email.register_email', compact('name', 'email','password', 'phone'),  function($message) use($use_email , $email_send, $email)
            {
                $message->from('autocheckmalaysia@gmail.com', 'Autocheck');
                $message->to($email)->subject('Registration');
            });

        

        if($check_email >= '1'){

            return redirect('/user')->with(['warning' => 'Email  already exist']);

        } 

         if($role == "NA"){

            $data             =  new User;
             
             $data->name       = $request->name;
             $data->email      = $request->email;
             $data->password   = $pass;
             $data->role_id    =  $request->role;
             $data->is_status_admin_group = $request->admin_status;
             $data->user_group_id = $request->user_group;
             $data->phone      =  $request->phone;
             $data->status     =  '1';
             $data->created_by =  $id_user;
            
            $data->save();

         }elseif($role == "KA"){

            $data             =  new User;
             
             $data->name       = $request->name;
             $data->email      = $request->email;
             $data->password   = $pass;
             $data->role_id    =  $request->role;
             $data->is_status_admin_group = $request->admin_status;
             $data->phone      =  $request->phone;
             $data->status     =  '1';
             $data->created_by =  $id_user;
            
            $data->save();

         }else{

             $data             =  new User;
             
             $data->name       = $request->name;
             $data->email      = $request->email;
             $data->password   = $pass;
             $data->role_id    =  $request->role;
             $data->phone      =  $request->phone;
             $data->status     =  '1';
             $data->created_by =  $id_user;
            
            $data->save();
        }

        $get_id = User::where('email', $email)->first();
        $is_id  = $get_id->id;

        $data          =  new DetailBuyer;
        $data->user_id = $is_id;
        $data->save();

        $data                       =  new HistoryUser;
        $data->user_id              = $is_id;
        $data->parameter_history_id = 'CREATE';
        $data->save();


        $use_email = 'autocheckmalaysia@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        /*Mail::send('web.register_email.register_email', compact('email_send', 'password', 'phone'),  function($message) use($use_email , $email_send)
            {
                $message->from($use_email, 'Autocheck');
                $message->to($email_send->email)->subject('Registration');
            });*/


        return redirect('/user')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = User::where('id',$id)->first();
        $role = Role::get();

        return view('admin.user.edit', compact('data', 'role'));
    }

    
    public function edit_group($id)
    {
        $data = User::where('id',$id)->first();
        $role = Role::get();

        return view('admin.user_group.edit', compact('data', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $name =  $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $role_id =  $request->input('role');

        $pass = bcrypt($password);


        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'password' => $pass, 'role_id' => $role_id, 'phone' => $phone));

         return redirect('user')->with(['success' => 'User data successfully updated']);
    }


    public function update_group(Request $request, $id)
    {

        $name =  $request->name;
        $email =  $request->email;
        $password =  $request->password;
        $phone =  $request->input('phone');
        $role_id =  $request->input('role');

        $pass = bcrypt($password);

        $data = User::where('id',$id)->update(array('name' => $name, 'email' => $email , 'password' => $pass, 'phone' => $phone));

         return redirect('user-group')->with(['success' => 'User data successfully updated']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('id',$id)->delete();   
        
        return redirect('user')->with(['success' => 'Data successfully deleted']);
    }


    public function destroy_group($id)
    {
        User::where('id',$id)->delete();   
        
        return redirect('user')->with(['success' => 'Data successfully deleted']);
    }

    

}
