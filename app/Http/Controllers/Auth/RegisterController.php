<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|min:3',
            'password' => 'required|string|min:6|confirmed',
            'address' => 'nullable|string',
            'fax' => 'nullable',
            'company_name' => 'nullable',
            'company_description' => 'nullable',
            'status_buyer' => 'nullable'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'status' => '1',
            'role_id' => 'US',
            'password' => Hash::make($data['password']),
        ]);*/

        $user = User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'phone' => $data['phone'],
                'address' => $data['address'],
                'fax' => $data['fax'],
                'company_name' => $data['company_name'],
                'company_description' => $data['company_description'],
                'role_id' => $data['status_buyer'],
                'password' => Hash::make($data['password']),
            ]);

           

        $user->userData = UserGroup::create([
            'group_name' => $data['company_name'],
        ]);

        return $user;
    }

    
    protected function register_user()
    {
        
        return view('auth.register_user', compact('data', 'role'));
        
    }
}
