<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Model\Role;
use App\Model\DetailBuyer;
use App\Model\HistoryUser;
use RealRashid\SweetAlert\Facades\Alert;
use Mail;
use Session;

class BuyerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $email    =  $request->input('email');
        $password =  $request->input('password');
        $pass     = bcrypt($password);
        $phone    =  $request->input('phone');
        $name     =  $request->input('name');

        $check_email = User::where('email', $email)->count();

        if($check_email > 0){


            Session::flash('name', $name);
            Session::flash('phone', $phone); 
            Session::flash('email', $email); 

            return redirect('/register')->with(['warning' => 'Email  already exist']);

        } 

         $data              =  new User;
         
         $data->name        = $request->name;
         $data->email       = $request->email;
         $data->password    = $pass;
         $data->role_id     =  'US';
         $data->phone       = $request->phone;
         $data->status      =  '1';
         $data->akses_buyer =  '1';
        $data->save();

        
        $get_id = User::where('email', $email)->first();
        $is_id  = $get_id->id;

        $data          =  new DetailBuyer;
        $data->user_id = $is_id;
        $data->save();

        $data                       =  new HistoryUser;
        $data->user_id              = $is_id;
        $data->parameter_history_id = 'CREATE';
        $data->save();

        /*$use_email = 'autocheckmalaysia2@gmail.com';

        $email_send = User::where('email', $email)->limit('1')->first();

        Mail::send('web.register_email.register_email', compact('email_send', 'password', 'phone'),  function($message) use($use_email , $email_send)
            {
                $message->from($use_email, 'Autocheck');
                $message->to($email_send->email)->subject('Registration');
            });*/


        return redirect('/login')->with(['success' => 'Data saved successfuly']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
