<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Model\DetailResult;
use App\Model\HistorySearching;
use RealRashid\SweetAlert\Facades\Alert;
use Session;
use Auth;
use GuzzleHttp\Client;
use Redirect;
use App\Model\Brand;
use App\Model\ParameterFuel;
use App\Model\ParameterSupplier;
use App\Model\ParameterTypeVehicle;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFee;
use App\Model\VehicleApi;
use App\Model\VehicleStatusMatch;
use App\Model\VehicleChecking;
use App\Model\ReportVehicle;
use App\Model\HistorySearchVehicle;
use App\Model\HistoryUser;
use App\Model\VehiclePast;
use App\Model\UserGroup;
use App\Model\HistoryBalance;
use App\User;
use App\Model\RSummary;
use App\Model\RUsageHistory;
use App\Model\RVehicleAssessment;
use App\Model\RVehicleDetails;
use App\Model\RVehicleSpecification;
use App\Model\RAuctionImages;
use App\Model\RDetailHistory;
use App\Model\RAuctionHistory;
use App\Model\ROdometerHistory;
use Ramsey\Uuid\Uuid;
use Carvx\CarvxService;
use Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Psr7\Stream;

class PayController extends Controller
{
    
    const USER_UID_HEADER = 'Carvx-User-Uid';

    private $url;
    private $uid = '34ll8i8hPxOY';
    private $key = 'fWU6b-pMs4DWTeSBJE4vcH3heskGEcXdrAFSBcZjV38yZld7DP4PKwXZ4co9lsJF';

    private $needSignature = true;
    private $raiseExceptions = false;
    private $isTest = false;


    public function __construct()

    {
        $this->middleware('auth');
    }


    public function store(Request $request)
    {   

        $id_user = Auth::user()->id;

        $chassisNumber = $request->carid;
        $id_vehicle     = Uuid::uuid4()->tostring();

        $role_me       = Auth::user()->role_id;


        $options=array(

                'needSignature' => '1', 
                'raiseExceptions' => '1',
                'isTest' => '0'
            );

        $url           = 'https://carvx.jp';
        $userUid       = '34ll8i8hPxOY';
        $apiKey        = 'yWgaN00IUCaUQhty-PbDtjhbL40E1-BBSeiiBR3Vqpvvz5Opk9zE2SYqpNNKzoDp';
        
        $service = new CarvxService($url, $userUid, $apiKey, $options);

        $search = $service->createSearch($chassisNumber);


        if(!empty($search->cars[0]->chassisNumber)){


                $is_sent = '1';
                $is_test = '0';

                $api_from = 'CARVX';

                $data                   =  new VehicleApi;
                $data->api_from         = $api_from;
                $data->istest           = $is_test; //
                $data->search_id        = $search->uid; //
                $data->car_id           = $search->cars[0]->carId; //
                $data->id_vehicle       = $id_vehicle;
                $data->vehicle          = $search->cars[0]->chassisNumber;
                $data->country_origin   = '';
                $data->brand            = $search->cars[0]->make;
                $data->model            = $search->cars[0]->model;
                $data->engine_number    = $search->cars[0]->engine;
                $data->cc               = '';
                $data->fuel_type        = '';
                $data->year_manufacture = $search->cars[0]->manufactureDate;
                $data->registation_date = '';
                $data->status           = 'found';
                $data->request_by       = $id_user;
                $data->save();


                $carId      = $search->cars[0]->carId;
                $searchId   = $search->uid;
                $isTest     = $is_test;

                
                /* ---------- create report API carvx ----------- */
                //$reportId   = $service->createReport($searchId, $carId, $isTest);  

                $reportId = 'dummy'; 

                $data                   =  new ReportVehicle;
                $data->id_vehicle       = $id_vehicle;
                $data->report_id        = $reportId;
                $data->save(); 

                
                $today  = date('HisdmY');
                $ver_sn = '00'.$api_from.$today;

                $data                   =  new VehicleStatusMatch;
                $data->id_vehicle = $id_vehicle;
                $data->vehicle = $search->cars[0]->chassisNumber;
                
                $data->ver_sn = $ver_sn;
                $data->save();

            }
    
        /* Create Searching */

        
        $status_api = '20'; //found
        $searching_by = 'API';
        
        /*table for naza / group*/

        $data                          =  new VehicleChecking;
        
        $data->id_vehicle              = $id_vehicle;
        $data->vehicle                 = $chassisNumber;
        $data->is_sent                 = '1';  
        
        $data->created_by              =  $id_user;
        $data->status                  =  $status_api;
        $data->searching_by            = $searching_by;
        $data->group_by                = $role_me;
        
        $data->save();

        /* End Create Searching */

        //history search
        $data                       =  new HistorySearchVehicle;
        
        $data->vehicle              = $request->vehicle;
        $data->id_vehicle           = $id_vehicle;
        $data->parameter_history_id = "SEARCH";
        $data->user_id              =  $id_user;
        
        $data->save();

        //history user            
        $data                       =  new HistoryUser;
        
        $data->vehicle_id           = $request->vehicle;
        $data->id_vehicle           = $id_vehicle;
        $data->parameter_history_id = "SEARCH";
        $data->user_id              =  $id_user;
        
        $data->save();


       


       $email_user = Auth::user()->email;
        

        $callback_url = url('/callback');
        $redirect_url = url('/callback');
        $client = new Client();
        $res = $client->request('POST', 'https://www.billplz.com/api/v3/bills', [

        
            'auth' => ['ee0c0a5a-8051-4d7e-861d-55c6285a7b7d', '', 'basic'],
            'form_params' => [
                'collection_id' => 'im6xvege6',
                'email' => 'wakudiallah05@gmail.com',
                'name' => 'User',
                'amount' => '20000',
                'mobile' => '0163824356',
                'callback_url' => $callback_url,
                'description' => 'Chassis Number : '.$search->cars[0]->chassisNumber,
                'redirect_url' => $redirect_url,
            ]
        ]); 



         //Staging
        /*$callback_url = url('/callback');
        $redirect_url = url('/callback');
        $client = new Client();
        $res = $client->request('POST', 'https://billplz-staging.herokuapp.com/api/v3/bills', [
        
            'auth' => ['d553e9ce-2c19-4f27-9763-a43900dd287e', '', 'basic'],
            'form_params' => [
                'collection_id' => 'vje0tvd4',
                'email' => 'wakudiallah05@gmail.com',
                'name' => 'User',
                'amount' => '200',
                'callback_url' => $callback_url,
                'description' => 'Chassis Number :'.$search->cars[0]->chassisNumber,
                'redirect_url' => $redirect_url,
            ]
        ]); */


        /* end staging */
          
          /*$attempt = Auth::user();
                $loglogin = new LogLogin;
                 
                $loglogin->email        = $user->email;
                $loglogin->id_user      = $user->id;
                $loglogin->name         = $applicant->fullname;
                $loglogin->ip           = $request->ip();
                $loglogin->lat          = $request->latitude;
                $loglogin->lng          = $request->longitude;
                $loglogin->location     = $request->location;
                $loglogin->ip           = $request->ip();
                $loglogin->activities   = 'Pelanggan semakan pembayaran online';
                $loglogin->id_applicant = $applicant->user_id;
                $loglogin->remark       = '141';
                $loglogin->type         = '2';
                $loglogin->save();*/
        //$purchase = Purchase::Where('id_purchase', $id_purchase)->
            //update([
              //"bill_id"   => 13,
              //"confirm_payment_date" => $today
           // ]);

        $array = json_decode($res->getBody()->getContents(), true);
        $var_url =  var_export($array['url'],true);
        $url = substr($var_url, 1, -1); 
        return Redirect::to($url);
    }

    

    public function callback(Request $request)
    { 
       
       return redirect('/dashboard')->with(['success' => 'Thank You, We will immediately process your request']);
    }
}
