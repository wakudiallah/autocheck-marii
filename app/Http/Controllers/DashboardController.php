<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;
use App\Model\VehicleChecking;
use App\Model\UserGroup;
use App\Model\VehicleStatusMatch;
use App\Model\HistoryUser;
use Auth;
use DB;
use DateTime;


class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
       
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $login = Auth::user()->role_id;
        $group_me = Auth::user()->role_id;


        /* User Group Chart*/
        $data_group_chart = UserGroup::get();

        $list_name_group_user =[];
        $list_name_group_total =[];

        foreach($data_group_chart as $datax){
          $list_name_group_user[] = $datax->group_name;
          $list_name_group_total[] = $datax->total_group_user->count();
        }
        /* End User Group Chart*/

       
        /* Chart Line (Manual Add)*/

        //naza//
        $line_chart_group = DB::table('user_groups')
        ->select(DB::raw('count(vehicle_checkings.vehicle) as totalx'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        
        ->join('vehicle_checkings','user_groups.user_id','=','vehicle_checkings.group_by')
        ->where('status', '40')
        ->where('group_by', 'NA')
        ->groupBy('month')
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->get();


          $list_line_chart = [];
          $list_line_chart_name = [];
          // loop all 12 month
          foreach(range(1, 12) as $month) {
             $flag = false; // init flag if no month found in montly assessment
             foreach($line_chart_group as $data) {
                if ($data->month == $month) { // if found add to the list
                    $list_line_chart [] = ($data->totalx)*200;
                    
                    $flag = true;
                    break; // break the loop once it found match result
                }
             }

             if(!$flag) {
                $list_line_chart [] = 0; // if not found, store as 0
             }
          }

          //Kastam
          //


        /*$line_chart_group_kastam = DB::table('user_groups')
        ->select(DB::raw('count(vehicle_checkings.vehicle) as totaly'), DB::raw('MONTH(vehicle_checkings.created_at) as monthw'))
        ->join('vehicle_checkings','user_groups.user_id','=','vehicle_checkings.group_by')
        ->whereIn('status', ['30','40'])
        ->where('group_by', 'KA')
        ->groupBy('monthx')
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->get();


          $list_line_chart_kastam = [];
          // loop all 12 month
          foreach(range(1, 12) as $month) {
             $flag = false; // init flag if no month found in montly assessment
             foreach($line_chart_group_kastam as $datar) {
                if ($datar->monthw == $month) { // if found add to the list
                    $line_chart_group_kastam [] = ($datar->totaly)*200;
                    $flag = true;
                    break; // break the loop once it found match result
                }
             }

             if(!$flag) {
                $line_chart_group_kastam [] = 0; // if not found, store as 0
             }
          }*/

        /* End Chart Line*/



        /* Line chart for date this month */
        
        $date = new DateTime('now');
        $date->modify('last day of this month');
        $last_date = $date->format('d');

        $array_date = DB::table('vehicle_checkings')
        ->select(DB::raw('count(vehicle) as totalx'), DB::raw('DATE_FORMAT(created_at, "%d") as dates'))
        
        ->groupBy('dates')
        ->where('status', '40')
        ->orderBy('dates','asc')
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->get();

        $array_date_kastam = DB::table('vehicle_checkings')
        ->select(DB::raw('count(vehicle) as totalx'), DB::raw('DATE_FORMAT(created_at, "%d") as dates'))
        ->where('group_by', 'KA')
        ->groupBy('dates')
        ->where('status', '40')
        ->orderBy('dates','asc')
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->get();


        $list_date_this_month_kastam = [];
        foreach(range(1, $last_date) as $last) {
             $flag = false; // init flag if no month found in montly assessment
             foreach($array_date_kastam as $data) {
                if ($data->dates == $last) { // if found add to the list
                    $list_date_this_month_kastam [] = $data->totalx;
                    
                    $flag = true;
                    break; // break the loop once it found match result
                }
             }

             if(!$flag) {
                $list_date_this_month_kastam [] = 0; // if not found, store as 0
             }
          }

        //dd($array_date);
         // exit();


        $list_date_this_month = [];
        foreach(range(1, $last_date) as $last) {
             $flag = false; // init flag if no month found in montly assessment
             foreach($array_date as $data) {
                if ($data->dates == $last) { // if found add to the list
                    $list_date_this_month [] = $data->totalx;
                    
                    $flag = true;
                    break; // break the loop once it found match result
                }
             }

             if(!$flag) {
                $list_date_this_month [] = 0; // if not found, store as 0
             }
          }


        /* --- Date 1-30*/
        $date_this_month = [];

        foreach (range(1, $last) as $date_month ) {
            $date_this_month[] = $date_month;
        }
        /* --- end Date 1-30*/

        /* end Line chart for date this month */


        $monthly_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        //->wherein('vehicle_checkings.stage', ['W2', 'W4', 'W100' ]) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
         ->get();


            $list_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_vehicle [] = 0; // if not found, store as 0
               }
            }
 
            



        /* API */

        $monthly_api = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.searching_by) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->wherein('vehicle_checkings.searching_by', ['API']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_api = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_api as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_api [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_api [] = 0; // if not found, store as 0
               }
            }


            /* NOT API */

        $monthly_not_api = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.searching_by) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->wherein('vehicle_checkings.searching_by', ['NOT']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_not_api = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_not_api as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_not_api [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_not_api [] = 0; // if not found, store as 0
               }
            }



        /* Pending vehicle */

        $monthly_pending_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->wherein('vehicle_checkings.status', ['10', '20']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_pending_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_pending_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_pending_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_pending_vehicle [] = 0; // if not found, store as 0
               }
            }



            /* Verify vehicle */

        $monthly_verify_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->wherein('vehicle_checkings.status', ['40']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_verify_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_verify_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_verify_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_verify_vehicle [] = 0; // if not found, store as 0
               }
            }



             /* Rejected vehicle */

        $monthly_rejected_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->wherein('vehicle_checkings.status', ['30']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_rejected_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_rejected_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_rejected_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_rejected_vehicle [] = 0; // if not found, store as 0
               }
            }


        /* -- Verify Vehicle / Group  Buyer -- */
        $monthly_verify_group_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('group_by', $group_me)
        ->wherein('vehicle_checkings.status', ['40']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_verify_group_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_verify_group_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_verify_group_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_verify_group_vehicle [] = 0; // if not found, store as 0
               }
            }




        /* -- Pending Vehicle / Group Buyer -- */
        $monthly_pending_group_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('group_by', $group_me)
        ->wherein('vehicle_checkings.status', ['10', '20' ]) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_pending_group_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_pending_group_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_pending_group_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_pending_group_vehicle [] = 0; // if not found, store as 0
               }
            }


        /* -- Rejected Vehicle / Group Buyer -- */
        $monthly_rejected_group_vehicle = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('group_by', $group_me)
        ->wherein('vehicle_checkings.status', ['30' ]) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_rejected_group_vehicle = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_rejected_group_vehicle as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_rejected_group_vehicle [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_rejected_group_vehicle [] = 0; // if not found, store as 0
               }
            }


            


        if($login == 'AD'){ //Admin
            
            $data = HistoryUser::orderBy('id', 'DESC')->take(5)->get();

            $total_vehicle = VehicleChecking::count(); //total vehicle
            $total_vehicle_verified = VehicleChecking::whereIn('status', ['30','40'])->count(); //verified by me
            $total_vehicle_pending = VehicleChecking::whereNotIn('status', ['30','40'])->count();
            
            $sum_balance = UserGroup::sum('balance');
            //pending where status
            //
            $balance = DB::table('user_groups')->sum('balance');



            return view('admin.dashboard', compact('data', 'total_vehicle', 'total_vehicle_pending', 'total_vehicle_verified', 'sum_balance'))
              ->with('list_name_group_user',json_encode($list_name_group_user, JSON_NUMERIC_CHECK))
            ->with('list_name_group_total',json_encode($list_name_group_total, JSON_NUMERIC_CHECK))
            ;



        }elseif($login == 'VER'){ //Ver
            $me = Auth::user()->id;

            $data = HistoryUser::orderBy('id', 'DESC')->take(5)->get();
            $total_vehicle = VehicleChecking::count(); //total vehicle
            $total_vehicle_verified_by_me = VehicleChecking::where('status', '40')->count(); //verified by me
            $total_vehicle_pending = VehicleChecking::whereNotIn('status', ['30','40'])->where('is_sent','1')->count(); //x di guna
            $total_reject_by_me = VehicleChecking::where('status', '30')->count();

            $total_pending_sent = VehicleChecking::whereNotIn('status', ['30','40'])->where('searching_by', 'NOT')->where('is_sent','0')->count();
            

            $total_open_sync_short_report = VehicleChecking::whereNotIn('status', ['25','30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('searching_by', 'API')->where('group_by', 'NA')->count();

            $total_open_sync_full_report = VehicleChecking::whereNotIn('status', ['25','30', '40'])->orderBy('id', 'DESC')->where('is_sent', '1')->where('searching_by', 'API')->where('group_by', 'KA')->count();


            return view('admin.verdashboard', compact('data','total_vehicle', 'total_vehicle_verified_by_me','date_this_month', 'total_vehicle_pending','total_reject_by_me', 'total_pending_sent','list_date_this_month_kastam','me', 'total_open_sync_short_report', 'total_open_sync_full_report'))
            ->with('list_pending_vehicle',json_encode($list_pending_vehicle, JSON_NUMERIC_CHECK))
            ->with('list_verify_vehicle',json_encode($list_verify_vehicle, JSON_NUMERIC_CHECK))
            ->with('date_this_month',json_encode($date_this_month, JSON_NUMERIC_CHECK))
            ->with('list_date_this_month',json_encode($list_date_this_month, JSON_NUMERIC_CHECK))

            ->with('list_date_this_month_kastam',json_encode($list_date_this_month_kastam, JSON_NUMERIC_CHECK))
            ->with('list_rejected_vehicle',json_encode($list_rejected_vehicle, JSON_NUMERIC_CHECK));




        }elseif($login == 'US'){ //User 

            $me = Auth::user()->id;
            
            $total_vehicle_verified = VehicleChecking::whereIn('status', ['30', '40'])->where('created_by', $me)->count();

            $total_vehicle_pending = VehicleChecking::whereIn('status', ['10', '20'])->where('created_by', $me)->count();

            return view('admin.usdashboard', compact('total_vehicle_pending','total_vehicle_verified'));
        }






        elseif($login == 'NA'){ //Group

            $cadealer = Auth::user()->user_group_id;

            $group_me = Auth::user()->role_id;
            $me = Auth::user()->id;

            $group_cadealer = User::where('user_group_id',$cadealer)->orderBy('last_login', 'DESC')->take(5)->get();

            //$group_cadealer = HistoryUser::orderBy('id', 'DESC')->take(5)->get();

            $total_vehicle = VehicleChecking::where('company_name', $cadealer)->count();
            $total_vehicle_verified = VehicleChecking::wherein('status', ['30','40'])->where('company_name', $cadealer)->count();  //jangan lupa where NAZA n oke semua 
            $total_vehicle_pending = VehicleChecking::wherein('status', ['10','20'])->where('company_name', $cadealer)->count();  //jangan lupa where NAZA n oke semua

            $me = Auth::user()->balance;
            $balance = UserGroup::where('user_id', $cadealer)->first();



            /* -- Verify Vehicle / Group  Buyer -- */
        $monthly_verify_group_cardealer = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('company_name', $cadealer)
        ->wherein('vehicle_checkings.status', ['40']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $group_vehicle_cadealer = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_verify_group_cardealer as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $group_vehicle_cadealer [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $group_vehicle_cadealer [] = 0; // if not found, store as 0
               }
            }


             /* -- Rejected Vehicle / Group Buyer -- */
        $monthly_rejected_group_cardealer = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('company_name', $cadealer)
        ->wherein('vehicle_checkings.status', ['30' ]) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_rejected_group_cardealer = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_rejected_group_cardealer as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_rejected_group_cardealer [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_rejected_group_cardealer [] = 0; // if not found, store as 0
               }
            }


            $monthly_pending_group_cardealer = DB::table('vehicle_checkings')
           ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
          ->groupBy('month')
        ->where('company_name', $cadealer)
          ->wherein('vehicle_checkings.status', ['10', '20' ]) 
          ->whereYear('vehicle_checkings.created_at', date('Y'))
          ->whereNull('deleted_at')
           ->get();


            $list_pending_group_cardealer = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_pending_group_cardealer as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_pending_group_cardealer [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_pending_group_cardealer [] = 0; // if not found, store as 0
               }
            }




            return view('admin.nadashboard', compact('group_cadealer', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending','balance'))
            ->with('group_vehicle_cadealer',json_encode($group_vehicle_cadealer, JSON_NUMERIC_CHECK))
             ->with('list_rejected_group_cardealer',json_encode($list_rejected_group_cardealer, JSON_NUMERIC_CHECK))
            ->with('list_pending_group_cardealer',json_encode($list_pending_group_cardealer, JSON_NUMERIC_CHECK));
        }






        elseif($login == 'KA'){

            $group_me = Auth::user()->role_id;
            $me = Auth::user()->id;
            $branch = Auth::user()->is_role_kastam;

            $user_kastam = User::where('role_id', $group_me)->orderBy('last_login', 'DESC')->take(5)->get();

            $total_vehicle = VehicleChecking::where('group_by', $group_me)->where('company_name', $branch)->count();

            $total_vehicle_verified = VehicleChecking::where('group_by', $group_me)->where('company_name', $branch)->whereIn('status', ['30','40'])->count();

            $total_vehicle_pending = VehicleChecking::where('group_by', $group_me)->where('company_name', $branch)->whereNotIn('status', ['30','40'])->count();


            /* -- Rejected Vehicle / Group Buyer -- */
        $monthly_rejected_kastam = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('group_by', $group_me)
        ->where('group_by', 'KA')
        ->where('company_name', $branch)
        ->wherein('vehicle_checkings.status', ['30' ]) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_rejected_group_kastam = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_rejected_kastam as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_rejected_group_kastam [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_rejected_group_kastam [] = 0; // if not found, store as 0
               }
            }


             $monthly_verify_kastam = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('group_by', 'KA')
        ->where('company_name', $branch)
        ->wherein('vehicle_checkings.status', ['40']) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_verify_kastam = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_verify_kastam as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_verify_kastam [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_verify_kastam [] = 0; // if not found, store as 0
               }
            }


             $monthly_pending_kastam = DB::table('vehicle_checkings')
         ->select(DB::raw('count(vehicle_checkings.status) as total'), DB::raw('MONTH(vehicle_checkings.created_at) as month'))
        ->groupBy('month')
        ->where('group_by', 'KA')
        ->where('company_name', $branch)
        ->wherein('vehicle_checkings.status', ['10', '20' ]) 
        ->whereYear('vehicle_checkings.created_at', date('Y'))
        ->whereNull('deleted_at')
         ->get();


            $list_pending_group_kastam = [];
            // loop all 12 month
            foreach(range(1, 12) as $month) {
               $flag = false; // init flag if no month found in montly assessment
               foreach($monthly_pending_kastam as $data) {
                  if ($data->month == $month) { // if found add to the list
                      $list_pending_group_kastam [] = $data->total;
                      $flag = true;
                      break; // break the loop once it found match result
                  }
               }

               if(!$flag) {
                  $list_pending_group_kastam [] = 0; // if not found, store as 0
               }
            }



            return view('admin.kadashboard', compact('user_kastam', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending'))
            ->with('list_verify_kastam',json_encode($list_verify_kastam, JSON_NUMERIC_CHECK))
             ->with('list_rejected_group_kastam',json_encode($list_rejected_group_kastam, JSON_NUMERIC_CHECK))
            ->with('list_pending_group_kastam',json_encode($list_pending_group_kastam, JSON_NUMERIC_CHECK));
        }






        elseif($login == 'MAN'){
            $data = HistoryUser::orderBy('id', 'DESC')->take(5)->get();

            $total_vehicle = VehicleChecking::count();
            $total_vehicle_verified = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua 
            $total_vehicle_pending = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua

 
            return view('admin.madashboard', compact('data', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending', 'list_date_this_month', 'list_date_this_month_kastam','date_this_month'))
            ->with('list_vehicle',json_encode($list_vehicle, JSON_NUMERIC_CHECK))
            ->with('list_api',json_encode($list_api, JSON_NUMERIC_CHECK))
            ->with('list_date_this_month',json_encode($list_date_this_month, JSON_NUMERIC_CHECK))
            ->with('date_this_month',json_encode($date_this_month, JSON_NUMERIC_CHECK))
             
            ->with('list_date_this_month_kastam',json_encode($list_date_this_month_kastam, JSON_NUMERIC_CHECK))
            ->with('list_not_api',json_encode($list_not_api, JSON_NUMERIC_CHECK));
        }


        elseif($login == 'MAR'){
            $data = User::orderBy('last_login', 'DESC')->take(5)->get();

            $total_vehicle = VehicleChecking::count();
            $total_vehicle_verified = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua 
            $total_vehicle_pending = VehicleStatusMatch::count();  //jangan lupa where NAZA n oke semua

            $total_group = UserGroup::count();
            $data_group = UserGroup::get();
            $data_group_chart = UserGroup::get();


            return view('admin.mardashboard', compact('data', 'total_vehicle', 'total_vehicle_verified', 'total_vehicle_pending', 'total_group', 'data_group', 'data_group_chart', 'line_chart_group'))
            ->with('list_vehicle',json_encode($list_vehicle, JSON_NUMERIC_CHECK))
            ->with('list_api',json_encode($list_api, JSON_NUMERIC_CHECK))
            ->with('list_not_api',json_encode($list_not_api, JSON_NUMERIC_CHECK))
            ->with('list_name_group_user',json_encode($list_name_group_user, JSON_NUMERIC_CHECK))
            ->with('list_name_group_total',json_encode($list_name_group_total, JSON_NUMERIC_CHECK))
            ->with('list_line_chart',json_encode($list_line_chart, JSON_NUMERIC_CHECK)) 
            ;
        }




    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
        

    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
