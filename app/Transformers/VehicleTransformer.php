<?php

namespace App\Transformers;

use App\MO;
use App\Model\Brand;
use App\Model\ParameterCountryOrigin;
use App\Model\ParameterModel;
use App\Model\ParameterFuel;
use App\Model\VehicleApi;
use App\Model\VehicleChecking;
use App\Model\VehicleManual;
use League\Fractal\TransformerAbstract;

class VehicleTransformer extends TransformerAbstract
{
	public function transform (VehicleChecking $mo)
	{
		
		if($mo->searching_by == 'API'){

			$vehicleapi = VehicleApi::where('id_vehicle', $mo->id_vehicle)->first();

			$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $mo->country_origin_id)->first();

			$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

			$regist_date =  date('F Y', strtotime($vehicleapi->registation_date));

			return[
				
				'status' => 'comity1',
				'id' => $mo->id,
				'vehicle' => $mo->vehicle,
				'country_origin' => $country_origin,
				'brand' => $vehicleapi->brand,
				'model' => $vehicleapi->model,
				'engine_model' => $mo->engine_number,
				'cc' => $vehicleapi->cc,
				'fuel' => $fuel,
				'year_manufacture' => $vehicleapi->year_manufacture,
				'first_registration_date' => $regist_date,  //Month YY
				//'token'=> $user->api_token,
			];

		}else{

			$vehiclemanual = VehicleManual::where('id_vehicle', $mo->id_vehicle)->first();

			$country_origin = $vehiclemanual->country_origin;
			$brand = $vehiclemanual->brand;
			$model = $vehiclemanual->model;
			$engine_number = $vehiclemanual->engine_number;
			$cc = $vehiclemanual->cc;
			$fuel = $vehiclemanual->fuel_type;
			$year_manufacture = $vehiclemanual->year_manufacture;
			$registation_date = $vehiclemanual->registation_date;



			$country_origin = ParameterCountryOrigin::addSelect('id','country_origin')->where('id', $country_origin)->first();

			$brand = Brand::addSelect('id','brand')->where('id', $brand)->first();

			$model = ParameterModel::addSelect('id','model')->where('id', $model)->first();

			$fuel = ParameterFuel::addSelect('id','fuel')->where('id', $mo->fuel_id)->first();

			$regist_date =  date('F Y ', strtotime($registation_date));
			

			return[
				
				'status' => 'comity2',
				'id'=> $mo->id,
				'vehicle' => $mo->vehicle,
				'country_origin' => $country_origin, //get from vehicle_checking 
				'brand' => $brand,
				'model' => $model,
				'engine_model'=> $mo->engine_number,  //get from vehicle_checking 
				'cc'=> $vehiclemanual->cc,  //get from vehicle_checking 
				'fuel'=> $fuel,
				'year_manufacture' => $year_manufacture,
				'first_registration_date' => $regist_date, //Month YY
				//'token'=> $user->api_token,
			];

		}
		

		
	}
}