<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RVehicleAssessment extends Model
{
    protected $table 	= 'r_vehicle_assessments';

	protected $guarded = ["id"]; 
}
