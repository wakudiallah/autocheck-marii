<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class RAuctionImages extends Model
{
    protected $table 	= 'r_auction_images';


	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
