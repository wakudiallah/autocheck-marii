<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RAccidentHistory extends Model
{
    
protected $table 	= 'r_accident_histories';

	protected $guarded = ["id"]; 
}
