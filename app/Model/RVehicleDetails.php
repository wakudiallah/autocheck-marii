<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;


class RVehicleDetails extends Model
{
    protected $table 	= 'r_vehicle_details';

	protected $guarded = ["id"]; 
}
