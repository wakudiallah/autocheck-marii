<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class HistorySearchVehicle extends Model
{
    protected $table 	= 'history_search_vehicles';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function historyuser() {
        return $this->belongsTo('App\User', 'user_id','id'); 
    }
}
