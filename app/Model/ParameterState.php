<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ParameterState extends Model
{
    protected $table 	= 'parameter_states';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
