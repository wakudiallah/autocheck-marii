<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class BatchSent extends Model
{
    protected $table 	= 'batch_sents';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function verify_sent_by() {
        return $this->belongsTo('App\User', 'created_by', 'id'); 
    }
}
