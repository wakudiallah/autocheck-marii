<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ParameterPackage extends Model
{

    protected $table 	= 'parameter_packages';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	public function create() {
        return $this->belongsTo('App\User', 'created_by','id'); 
    }

}
