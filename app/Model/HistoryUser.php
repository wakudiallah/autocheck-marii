<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class HistoryUser extends Model
{
    
	protected $table 	= 'history_users';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function history_per_user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id')->latest();
    }

    public function status_case()
    {
        return $this->belongsTo('App\Model\VehicleChecking', 'id_vehicle', 'id_vehicle')->latest();
    }
}

