<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class HistoryBalance extends Model
{
    protected $table 	= 'history_balances';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	public function topup_by() {
        return $this->belongsTo('App\User', 'created_by', 'id'); 
    }

    public function vehicle_c() {
        return $this->belongsTo('App\Model\VehicleChecking', 'id_vehicle', 'id_vehicle'); 
    }

   
}
