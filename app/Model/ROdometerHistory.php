<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ROdometerHistory extends Model
{
    protected $table 	= 'r_odometer_histories';

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
