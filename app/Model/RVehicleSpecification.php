<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RVehicleSpecification extends Model
{
    protected $table 	= 'r_vehicle_specifications';

	protected $guarded = ["id"]; 
}
