<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RAuctionHistory extends Model
{
    protected $table 	= 'r_auction_histories';


	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
