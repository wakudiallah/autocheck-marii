<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class RUsageHistory extends Model
{
    protected $table 	= 'r_usage_histories';

	protected $guarded = ["id"]; 
}
