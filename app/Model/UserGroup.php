<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class UserGroup extends Model
{
    protected $table 	= 'user_groups';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	protected $fillable = [
        'group_name', 'user_id'
    ];

	public function total_group_user() {
        return $this->hasMany('App\User', 'user_group_id','user_id');    
    }

   
}
