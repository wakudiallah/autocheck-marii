<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class ParameterPostcode extends Model
{
    protected $table 	= 'parameter_postcodes';


    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;


	public function state() {
		return $this->belongsTo('App\Model\ParameterState','state_code','state_code');	
		//Postcode dipunyai city
	}
}
