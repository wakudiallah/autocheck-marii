<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class ReportVehicle extends Model
{
    protected $table 	= 'report_vehicles';


	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
