<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class VehicleManual extends Model
{
    protected $table 	= 'vehicle_manuals';

    use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;

	 public function manual_brand() {
        return $this->belongsTo('App\Model\Brand', 'brand','id'); 
    }

    public function manual_model_brand() {
        return $this->belongsTo('App\Model\ParameterModel', 'model','id'); 
    }

     public function manual_fuel() {
        return $this->belongsTo('App\Model\ParameterFuel', 'fuel_type','id'); 
    }
}
